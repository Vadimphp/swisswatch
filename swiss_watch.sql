-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Час створення: Сер 27 2017 р., 10:05
-- Версія сервера: 5.6.37
-- Версія PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `swiss_watch`
--

-- --------------------------------------------------------

--
-- Структура таблиці `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `brand_name` varchar(49) NOT NULL,
  `mail` enum('man','woman','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `brands`
--

INSERT INTO `brands` (`id`, `brand_name`, `mail`) VALUES
(1, 'Tissot', 'man'),
(2, 'Edox', 'man'),
(3, 'Raymond Weil', 'man'),
(4, 'Victorinox Swiss Army', 'man'),
(5, 'Maurice Lacroix', 'man');

-- --------------------------------------------------------

--
-- Структура таблиці `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(49) NOT NULL,
  `text` varchar(255) NOT NULL,
  `rating` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `comment`
--

INSERT INTO `comment` (`id`, `product_id`, `name`, `text`, `rating`) VALUES
(1, 1, 'Zack', 'Came in a beautiful package, great view, the color of the arrow, chronometer, all priceless.', 5),
(2, 1, 'Ron', 'Everything is cool! It is the best gift', 5),
(3, 2, 'Dima', 'In this model, all original. The phenomenal thing.', 5),
(4, 3, 'Oleg', 'The clock even today relevant. The appearance, shape, design, speed is at a height.', 5),
(5, 4, 'Petr', 'Great watch for the price. I advise everyone!', 5),
(16, 5, 'Vadim', 'super watch', 0),
(19, 21, 'Lili', 'Nice watch', 0),
(20, 1, 'Укр', 'Wery good watch', 0),
(21, 5, 'Alla', 'Cool watch \r\n)))', 0),
(22, 5, 'ert\\ik', 'well well well', 0),
(23, 2, 'rrr', 'tth ggggggggggggg', 0),
(24, 5, 'ertic', 'whera i can by it in Odessa', 0),
(25, 19, 'redij', 'yoooooooooooooo\r\n', 0);

-- --------------------------------------------------------

--
-- Структура таблиці `compare`
--

CREATE TABLE `compare` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `session` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `compare`
--

INSERT INTO `compare` (`id`, `product_id`, `session`) VALUES
(1, 1, ''),
(3, 5, ''),
(4, 9, ''),
(6, 18, ''),
(7, 20, ''),
(8, 3, '');

-- --------------------------------------------------------

--
-- Структура таблиці `description`
--

CREATE TABLE `description` (
  `id` int(11) NOT NULL,
  `specification` text NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `description`
--

INSERT INTO `description` (`id`, `specification`, `description`) VALUES
(1, '<p>Manufacturer: Switzerland / Movement: mechanical with automatic winding /</p>\r\n\r\n<p>Glass: sapphire / Showing Type: Switches / housing: steel /</p>\r\n\r\n<p>resistance: 30 m / Strap: Leather / Type: Male</p>\r\n', '<p>Sapphire glass with anti-reflective coating. Caliber mechanism: ETA 2824-2. In the mechanism - a 25 gemstones. The model refers to the classic line of Le Locle. Guilloche dial.</p>\r\n'),
(2, 'Manufacturer: Switzerland / mechanism: quartz / glass: sapphire / Showing Type: Switches / housing: steel / water resistance: 100 meters / Strap: Leather / Type: Male', 'You can include both data and clock to the classic and to the sporting style. At the same time, the model trend and called Couturier Quartz. The brown leather strap, stainless steel case. The distance between the fastening lugs - 23 mm. Dial color - silver. Type of dial - indices. there are 30 minute and 1/10 second counters in this model. The functions of addition and split, allowing pinpoint simultaneously 2 different length of time. There is also a tachymeter, allowing the owner to measure the average speed (in kilometers per hour) on a fixed track section using the chronological clock function. Established Swiss quartz movement ETA G10.211 4 gems'),
(3, 'Manufacturer: Switzerland. Mechanism: quartz. Housing: steel. Material: steel. Strap: leather. Glass: sapphire. Water resistance: 200 meters', 'Dial color: blue. Indexing: Indexes'),
(4, 'Manufacturer: Switzerland. Type: men. Strap: rubber. water resistance: 200 meters. Housing: steel. Type of display: Switches. Glass: sapphire. Movement: mechanical with automatic winding', 'Used Swiss movement: ETA C01.211. The body is made of steel. Black rubber strap. Dial color - black. Indications - Arabic numerals and labels. At the arrows and labels luminescent coating that provides long-term illumination in the dark. Crown and caseback screw. Buckle Strap - \"Butterfly\" with a double fold.'),
(5, 'Manufacturer: Switzerland. Movement: mechanical with automatic winding. Glass: sapphire. Showing Type: Switches. Housing: steel. Resistance: 30 m. Strap: Leather. Type: Male', 'Sapphire glass with anti-reflective coating. Caliber mechanism: C01.211. Transparent back cover. The mechanism - 15 stones. Power reserve 45 hours. The model belongs to the line of Tissot Carson. Stopwatch on 30 minutes and 1/10 of a second counter, central seconds hand performs the function of the chronograph.'),
(6, 'Manufacturer: Switzerland. Movement: mechanical with automatic winding. Glass: sapphire. Showing Type: Switches. Housing: steel. Water resistance: 200 meters. Bracelet: steel. Type: Male3', 'Watches are 200 lineup PRC and made in sporty style, suitable for divers, thanks water resistance 200 m. Housing and stainless steel bracelet. Caliber mechanism - C01.211 15 jewels. Central 60-second chronograph and 30-minute and 6-hour counters. Power reserve up to 45 hours. Screw-down crown. Transparent back cover. Dial color - black. Type of dial - indices.'),
(7, 'Manufacturer: Switzerland. Mechanism: Mechanical. Housing: steel. Material: steel. Strap: leather. Glass: sapphire. Water resistance: 100 meters. Type: Male', 'Self-winding: Yes, the mechanism name: ETA 7750 Dial color: black indexation: index, body length: 39.8 mm'),
(8, 'Manufacturer: Switzerland. Mechanism: Mechanical. Strap: leather. Glass: sapphire. Water resistance: 100 meters. Type: Male', 'Self-winding: Yes. Dial color: black. Indexing: Indexes'),
(9, 'Manufacturer: Switzerland. Mechanism: quartz. Glass: sapphire. Type of display: Switches. Housing: steel + PVD. Water resistance: 100 meters. Strap: caoutchouc. Type: Male', 'Luminescent hands. Sapphire glass with anti-reflective coating. Caliber mechanism: Edox 103, on the basis of Ronda 5021.D. The bezel is covered with a black PVD-coated. Band Width - 23 mm. Guilloche dial.'),
(10, 'Manufacturer: Switzerland. Strap: leather. Water resistance: 50 m. Housing: steel. Showing Type: Switches. Glass: sapphire. Movement: mechanical with automatic winding. Type: Male', 'Rally Timer Day Date Automatic. Caliber: 83 based on the ETA 2834. Indication: seconds, minutes, hours, date, day of the week. Guilloche dial. Housing made of high-quality steel alloy 316L with high anti-corrosion properties. Folding steel clasp with Edox logo. Case size: diameter - 42 mm, thickness 11.3 mm.'),
(11, 'Manufacturer: Switzerland. Mechanism: quartz. Glass: sapphire. Showing Type: Switches. Housing: steel. Water resistance: 100 meters. Bracelet: steel. Type: Male', 'Hands with luminescent coating. Sapphire glass with anti-reflective coating. Model 2012. Limited Edition. On the dial engraved map.'),
(12, 'Manufacturer: Switzerland. Movement: mechanical with automatic winding. Glass: sapphire. Showing Type: Switches. body: steel with gold. water resistance: 50 m. Strap: Leather. Type: Male', 'Partially open mechanism. Sapphire glass with anti-reflective coating.'),
(13, 'Manufacturer: Switzerland. Mechanism: Mechanical. Housing: steel. Material: steel. Strap: leather. Glass: sapphire. Water resistance: 50 m. Type: Male', 'Self-winding: Yes, the mechanism name: RAYMOND WEIL, Dial color: silver, Indexing: Roman numerals, body length: 45.6 mm'),
(14, 'Manufacturer: Switzerland. Mechanism: Mechanical. Housing: steel. Material: steel. Strap: leather. Glass: sapphire. Water resistance: 50 m. Type: Male', 'Self-winding: Yes. The mechanism name: RAYMOND WEIL, Dial color: black. Indexing: Roman numeral. Anti-reflective: Yes'),
(15, 'Manufacturer: Switzerland. Mechanism: quartz. Glass: sapphire. Showing Type: Switches. Housing: steel + PVD. Water resistance: 100 meters. Strap: Leather. Type: Male', 'Caliber mechanism: ETA G10.211. The watch case is made of steel. Bezel with PVD coated black. Black leather strap. Dial color - black. Indications - Arabic numerals and labels. A luminous coating on the arrows and labels, provides long-term illumination in the dark. Sapphire glass with anti-reflective coating. The back cover with screw clamps. Buckle Strap - Buckle.'),
(16, 'Manufacturer: Switzerland. Mechanism: quartz. Glass: sapphire. Showing Type: Switches. Housing: steel + PVD. Water resistance: 100 meters. Bracelet: steel. Type: Male', 'Caliber mechanism: ETA G10.211. The case and bracelet watch is made of steel. Bezel PVD-coated blue. Dial color - blue. Indications - Arabic numerals and labels. A luminous coating on the arrows and labels, provides long-term illumination in the dark. Sapphire glass with anti-reflective coating. The back cover with screw clamps. Clasp bracelet with double fold.'),
(17, 'Manufacturer: Switzerland. Mechanism: quartz. Glass: sapphire. Showing Type: Switches. Housing: steel + PVD. Water resistance: 500 meters. Bracelet: Steel + PVD. Type: Male', 'Caliber mechanism: ETA G10.211. The case and bracelet watches are made of steel with PVD coated black. Dial color - orange. Indication - mark. A luminous coating on the arrows and labels, provides long-term illumination in the dark. The back cover and crown with screw clamps. The bezel rotates counterclockwise. Clasp bracelet with double fold.'),
(18, 'Manufacturer: Switzerland. Movement: mechanical with automatic winding. Glass: sapphire. Showing Type: Switches. Housing: steel + PVD. Water resistance: 500 meters. Strap: caoutchouc. Type: Male', 'Caliber mechanism: ETA 2892-A2. The watch case is made of steel with PVD coated black. Brown rubber strap. Dial color - brown. Indication - mark. A luminous coating on the arrows and labels, provides long-term illumination in the dark. The back cover and crown with screw clamps. The bezel rotates counterclockwise. Buckle Strap - Buckle.'),
(19, 'Manufacturer: Switzerland. Mechanism: quartz. Housing: steel. Material: steel. Strap: leather. Glass: sapphire. Water resistance: 30 m. Type: Male', 'The name of the mechanism: Ronda 5040.F. Dial color: white, Indexing: Indexes. Time Format: 12 hour.'),
(20, 'Manufacturer: Switzerland. Mechanism: Mechanical. Housing: steel. Material: steel. Bracelet: steel. Glass: sapphire. Water resistance: 30 m. Type: Male', 'Self-winding: Yes. the mechanism name: ML 115. Dial color: gray. Indexing: Indexes. Time Format: 12 hour.'),
(21, 'Manufacturer: Switzerland. Mechanism: quartz. Glass: sapphire. Showing Type: Switches. Housing: steel + PVD. Resistance: 30 m. Strap: caoutchouc. Type: Ladies', 'Caliber mechanism: Ronda 4210 B. The case is encrusted with 38 stones zirconium. The arrows and numbers from superlyuminova coating. Anti-glare Sapphire glass');

-- --------------------------------------------------------

--
-- Структура таблиці `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `filePath` varchar(400) NOT NULL,
  `itemId` int(11) DEFAULT NULL,
  `isMain` tinyint(1) DEFAULT NULL,
  `modelName` varchar(150) NOT NULL,
  `urlAlias` varchar(400) NOT NULL,
  `name` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `image`
--

INSERT INTO `image` (`id`, `filePath`, `itemId`, `isMain`, `modelName`, `urlAlias`, `name`) VALUES
(26, 'Products/Product1/e61924.jpg', 1, 1, 'Product', '2e8d90ea5b-3', ''),
(27, 'Products/Product2/127f41.jpg', 2, 1, 'Product', 'c72a09b7ca-1', ''),
(28, 'Products/Product3/4b78f0.jpg', 3, 1, 'Product', 'f8011ea65c-1', ''),
(29, 'Products/Product4/9bc6d1.jpg', 4, 1, 'Product', '7227a626df-1', ''),
(30, 'Products/Product5/2969bb.jpg', 5, 1, 'Product', '3dd3b0d3f9-1', ''),
(31, 'Products/Product6/c70326.jpg', 6, 1, 'Product', '68d95b58d5-1', ''),
(32, 'Products/Product7/1191cd.jpg', 7, 1, 'Product', 'ab9ed8b7d4-1', ''),
(33, 'Products/Product8/579667.jpg', 8, 1, 'Product', 'cfe16d2fde-1', ''),
(34, 'Products/Product9/3af264.jpg', 9, 1, 'Product', '2ef92df1d3-1', ''),
(35, 'Products/Product10/205fcc.jpg', 10, 1, 'Product', '431913547a-1', ''),
(36, 'Products/Product11/536101.jpg', 11, 1, 'Product', 'bceb04bb29-1', ''),
(37, 'Products/Product12/68e187.jpg', 12, 1, 'Product', 'fc17198339-1', ''),
(38, 'Products/Product13/939971.jpg', 13, 1, 'Product', '6c2d5d73ca-1', ''),
(39, 'Products/Product14/65d1a3.jpg', 14, 1, 'Product', 'e953fd70e8-1', ''),
(40, 'Products/Product15/352a55.jpg', 15, 1, 'Product', '001cef2419-1', ''),
(41, 'Products/Product16/c94571.jpg', 16, 1, 'Product', '65465054c4-1', ''),
(42, 'Products/Product17/e641c7.jpg', 17, 1, 'Product', 'abb115aefb-1', ''),
(43, 'Products/Product18/73957d.jpg', 18, 1, 'Product', '677bc24abe-1', ''),
(44, 'Products/Product19/eacd2f.jpg', 19, 1, 'Product', 'fb8f9daf32-1', ''),
(45, 'Products/Product20/7c78af.jpg', 20, 1, 'Product', 'f127a90c6b-1', ''),
(46, 'Products/Product21/6b2862.jpg', 21, 1, 'Product', '80673ca90e-1', ''),
(62, 'Products/Product1/a0fce4.jpg', 1, NULL, 'Product', 'a11437ce2a-2', ''),
(63, 'Products/Product1/760f1c.jpg', 1, NULL, 'Product', 'f0b009cfc7-3', ''),
(64, 'Products/Product1/275977.jpg', 1, NULL, 'Product', '42ab815e91-4', ''),
(65, 'Products/Product1/f57778.jpg', 1, NULL, 'Product', '8de6071db4-5', ''),
(66, 'Products/Product2/4f729b.jpg', 2, NULL, 'Product', '6ffcf87cfb-2', ''),
(67, 'Products/Product2/10ee43.jpg', 2, NULL, 'Product', '1edf3dd573-3', ''),
(68, 'Products/Product2/9ee2a6.jpg', 2, NULL, 'Product', '8cfb64d05d-4', ''),
(69, 'Products/Product2/3b23e9.jpg', 2, NULL, 'Product', 'f57fcc62c7-5', ''),
(70, 'Products/Product3/cb1cab.jpg', 3, NULL, 'Product', '80b422f627-2', ''),
(71, 'Products/Product3/a484f2.jpg', 3, NULL, 'Product', 'a2ca133689-3', ''),
(72, 'Products/Product3/4768e4.jpg', 3, NULL, 'Product', 'ad8ee89b22-4', ''),
(73, 'Products/Product3/cf0b18.jpg', 3, NULL, 'Product', '440aab60d5-5', ''),
(74, 'Products/Product4/e9fc6d.jpg', 4, NULL, 'Product', '435c53ac7f-2', ''),
(75, 'Products/Product4/7a45c6.jpg', 4, NULL, 'Product', '1cce5fed28-3', ''),
(76, 'Products/Product4/2119a4.jpg', 4, NULL, 'Product', '6cf9619a5b-4', ''),
(77, 'Products/Product4/f58bc5.jpg', 4, NULL, 'Product', '36acbde744-5', ''),
(78, 'Products/Product5/0cedf3.jpg', 5, NULL, 'Product', '103a600061-2', ''),
(79, 'Products/Product5/947a42.jpg', 5, NULL, 'Product', 'caeb555814-3', ''),
(80, 'Products/Product5/3ce8c5.jpg', 5, NULL, 'Product', 'b2425d655e-4', ''),
(81, 'Products/Product5/16755a.jpg', 5, NULL, 'Product', '5adc2de3b9-5', ''),
(86, 'Products/Product7/c323fe.jpg', 7, NULL, 'Product', '4ec4c24722-2', ''),
(87, 'Products/Product7/4f72bd.jpg', 7, NULL, 'Product', 'df4888c17f-3', ''),
(88, 'Products/Product7/a5a10f.jpg', 7, NULL, 'Product', 'd25878c663-4', ''),
(89, 'Products/Product7/c0e62a.jpg', 7, NULL, 'Product', '152daae629-5', ''),
(90, 'Products/Product8/a31207.jpg', 8, NULL, 'Product', '83b19566f3-2', ''),
(91, 'Products/Product8/e4ca39.jpg', 8, NULL, 'Product', '3c62e394e1-3', ''),
(92, 'Products/Product8/1a8215.jpg', 8, NULL, 'Product', '8d6c2f4ac2-4', ''),
(93, 'Products/Product8/69332c.jpg', 8, NULL, 'Product', '64b97e9166-5', ''),
(94, 'Products/Product9/1984a0.jpg', 9, NULL, 'Product', '0954479405-2', ''),
(95, 'Products/Product9/3c3df6.jpg', 9, NULL, 'Product', 'b99222ecfd-3', ''),
(96, 'Products/Product9/d1a9ce.jpg', 9, NULL, 'Product', '39787c6817-4', ''),
(97, 'Products/Product9/0bdf42.jpg', 9, NULL, 'Product', 'ba17f8127a-5', ''),
(98, 'Products/Product6/b58b38.jpg', 6, NULL, 'Product', '11f2de51da-2', ''),
(99, 'Products/Product6/953efa.jpg', 6, NULL, 'Product', '5abc6a7211-3', ''),
(100, 'Products/Product6/6699a0.jpg', 6, NULL, 'Product', '6b5437c15e-4', ''),
(101, 'Products/Product6/58fc6e.jpg', 6, NULL, 'Product', '15f6d80ae4-5', ''),
(102, 'Products/Product10/5d7966.jpg', 10, NULL, 'Product', '7f37c13908-2', ''),
(103, 'Products/Product10/64dfa1.jpg', 10, NULL, 'Product', 'ee7cd30855-3', ''),
(104, 'Products/Product10/6fdc44.jpg', 10, NULL, 'Product', 'b150a72100-4', ''),
(105, 'Products/Product10/a3abc9.jpg', 10, NULL, 'Product', 'd562c184a6-5', ''),
(106, 'Products/Product11/26eb4b.jpg', 11, NULL, 'Product', '762e796967-2', ''),
(107, 'Products/Product11/202de0.jpg', 11, NULL, 'Product', 'f19ffc1e39-3', ''),
(108, 'Products/Product11/f1796f.jpg', 11, NULL, 'Product', '9b0240f56b-4', ''),
(109, 'Products/Product11/a94bbb.jpg', 11, NULL, 'Product', '8fdee845cc-5', ''),
(110, 'Products/Product12/ac7d07.jpg', 12, NULL, 'Product', '0765892ae8-2', ''),
(111, 'Products/Product12/9e9d9a.jpg', 12, NULL, 'Product', '9f76515581-3', ''),
(112, 'Products/Product12/9d6b68.jpg', 12, NULL, 'Product', '8afb96b664-4', ''),
(113, 'Products/Product12/a9beef.jpg', 12, NULL, 'Product', 'dfbb20a429-5', ''),
(114, 'Products/Product13/0c15e1.jpg', 13, NULL, 'Product', '4eb974e064-2', ''),
(115, 'Products/Product13/8b31af.jpg', 13, NULL, 'Product', '6716d6554a-3', ''),
(116, 'Products/Product13/d96e24.jpg', 13, NULL, 'Product', '8a6ebe9063-4', ''),
(117, 'Products/Product13/54a0fd.jpg', 13, NULL, 'Product', '2e9fc3e90b-5', ''),
(118, 'Products/Product14/fdaa6f.jpg', 14, NULL, 'Product', 'ded02bb4a6-2', ''),
(119, 'Products/Product14/41debc.jpg', 14, NULL, 'Product', '589bfab9cd-3', ''),
(120, 'Products/Product14/f4f279.jpg', 14, NULL, 'Product', 'a11d226217-4', ''),
(121, 'Products/Product14/6feb72.jpg', 14, NULL, 'Product', '0eec406a97-5', ''),
(122, 'Products/Product15/c28d59.jpg', 15, NULL, 'Product', '22f3ac7f67-2', ''),
(123, 'Products/Product15/b46e8a.jpg', 15, NULL, 'Product', 'ef42603b41-3', ''),
(124, 'Products/Product15/83c2b4.jpg', 15, NULL, 'Product', '474c4a7965-4', ''),
(125, 'Products/Product15/b941a7.jpg', 15, NULL, 'Product', '0d169ad92f-5', ''),
(126, 'Products/Product16/ca169f.jpg', 16, NULL, 'Product', 'a663aee0b8-2', ''),
(127, 'Products/Product16/54c24c.jpg', 16, NULL, 'Product', 'f1ef0facee-3', ''),
(128, 'Products/Product16/c316de.jpg', 16, NULL, 'Product', '9e7352bbe9-4', ''),
(129, 'Products/Product16/60e6bb.jpg', 16, NULL, 'Product', '1a26d0fcb0-5', ''),
(130, 'Products/Product17/12caa6.jpg', 17, NULL, 'Product', '898c643f5c-2', ''),
(131, 'Products/Product17/afd5c2.jpg', 17, NULL, 'Product', '3c7b99936a-3', ''),
(132, 'Products/Product17/fa6ea6.jpg', 17, NULL, 'Product', 'b72fb299aa-4', ''),
(133, 'Products/Product17/482721.jpg', 17, NULL, 'Product', '5b6d1fe360-5', ''),
(134, 'Products/Product18/b7a766.jpg', 18, NULL, 'Product', '0b3e1a98d7-2', ''),
(135, 'Products/Product18/e72ebb.jpg', 18, NULL, 'Product', '92ade97225-3', ''),
(136, 'Products/Product18/190329.jpg', 18, NULL, 'Product', '945911c6bc-4', ''),
(137, 'Products/Product18/550451.jpg', 18, NULL, 'Product', '2e5b41a27d-5', ''),
(138, 'Products/Product19/79b95b.jpg', 19, NULL, 'Product', '1aac16ca08-2', ''),
(139, 'Products/Product19/2f2415.jpg', 19, NULL, 'Product', '3ad0e88ff6-3', ''),
(140, 'Products/Product19/438f16.jpg', 19, NULL, 'Product', 'f992a66d4f-4', ''),
(141, 'Products/Product19/309056.jpg', 19, NULL, 'Product', '2ce8fa5c91-5', ''),
(142, 'Products/Product21/e41417.jpg', 21, NULL, 'Product', 'b9b4baec6e-2', ''),
(143, 'Products/Product21/318b02.jpg', 21, NULL, 'Product', '460985d58d-3', ''),
(144, 'Products/Product21/9313d4.jpg', 21, NULL, 'Product', '03050b50ef-4', ''),
(145, 'Products/Product21/c20648.jpg', 21, NULL, 'Product', 'a0885e828b-5', ''),
(146, 'Products/Product20/8030d8.jpg', 20, NULL, 'Product', 'edc1cd241f-2', ''),
(147, 'Products/Product20/cc7fd1.jpg', 20, NULL, 'Product', 'b87f149288-3', ''),
(148, 'Products/Product20/060d22.jpg', 20, NULL, 'Product', '6a7528462d-4', ''),
(149, 'Products/Product20/156cb4.jpg', 20, NULL, 'Product', '1e836c1bca-5', '');

-- --------------------------------------------------------

--
-- Структура таблиці `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1483636869),
('m130524_201442_init', 1483636881),
('m140622_111540_create_image_table', 1483957797),
('m140622_111545_add_name_to_image_table', 1483957797);

-- --------------------------------------------------------

--
-- Структура таблиці `option`
--

CREATE TABLE `option` (
  `id` int(11) NOT NULL,
  `option_name` varchar(49) NOT NULL,
  `sort_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `option`
--

INSERT INTO `option` (`id`, `option_name`, `sort_order`) VALUES
(1, 'Manufacturer', 1),
(2, 'Display_type', 2),
(3, 'Mechanism', 3),
(4, 'Type', 4),
(5, 'Housing', 5),
(6, 'Water resistance, m', 6),
(7, 'Glass', 7),
(8, 'Strap', 8),
(9, 'Width, mm', 9),
(10, 'Weight, g', 10),
(11, 'Height, mm', 11),
(12, 'Thickness, mm', 12),
(13, 'Functions date', 13),
(14, 'Functions chronograph', 14),
(15, 'Functions tachymeter', 15);

-- --------------------------------------------------------

--
-- Структура таблиці `order_item`
--

CREATE TABLE `order_item` (
  `id` int(11) UNSIGNED NOT NULL,
  `order_id` int(11) UNSIGNED NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `brand_name` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `quantity_item` int(11) NOT NULL,
  `sum_item` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `order_item`
--

INSERT INTO `order_item` (`id`, `order_id`, `product_id`, `brand_name`, `price`, `quantity_item`, `sum_item`) VALUES
(1, 1, 3, '<p>Tissot</p>\r\n', 13289, 1, 13289),
(2, 1, 9, '<p>Edox</p>\r\n', 30125, 1, 30125),
(3, 2, 14, '<p>Raymond Weil</p>\r\n', 35240, 3, 105720),
(4, 3, 5, '<p>Tissot</p>\r\n', 22749, 5, 113745),
(5, 4, 1, 'Tissot', 14429, 1, 14429);

-- --------------------------------------------------------

--
-- Структура таблиці `photo`
--

CREATE TABLE `photo` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `path_to_file` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `photo`
--

INSERT INTO `photo` (`id`, `product_id`, `path_to_file`, `sort_order`) VALUES
(1, 1, '/uploads/man/01_tissot_t41/t1.jpg', 1),
(2, 1, '/uploads/man/01_tissot_t41/t2.jpg', 2),
(3, 1, '/uploads/man/01_tissot_t41/t3.jpg', 3),
(4, 1, '/uploads/man/01_tissot_t41/t4.jpg', 4),
(5, 2, '/uploads/man/02_tissot_t035/t1.jpg', 1),
(6, 2, '/uploads/man/02_tissot_t035/t2.jpg', 2),
(7, 2, '/uploads/man/02_tissot_t035/t3.jpg', 3),
(8, 2, '/uploads/man/02_tissot_t035/t4.jpg', 4),
(9, 3, '/uploads/man/03_tissot_t055/t1.jpg', 1),
(10, 3, '/uploads/man/03_tissot_t055/t2.jpg', 2),
(11, 3, '/uploads/man/03_tissot_t055/t3.jpg', 3),
(12, 3, '/uploads/man/03_tissot_t055/t4.jpg', 4),
(13, 4, '/uploads/man/04_tissot_t055_427/t1.jpg', 1),
(14, 4, '/uploads/man/04_tissot_t055_427/t2.jpg', 2),
(15, 4, '/uploads/man/04_tissot_t055_427/t3.jpg', 3),
(16, 4, '/uploads/man/04_tissot_t055_427/t4.jpg', 4),
(17, 5, '/uploads/man/05_tissot_t068_427/t1.jpg', 1),
(18, 5, '/uploads/man/05_tissot_t068_427/t2.jpg', 2),
(19, 5, '/uploads/man/05_tissot_t068_427/t3.jpg', 3),
(20, 5, '/uploads/man/05_tissot_t068_427/t4.jpg', 4),
(21, 6, '/uploads/man/06_tissot_t014/t1.jpg', 1),
(22, 6, '/uploads/man/06_tissot_t014/t2.jpg', 2),
(23, 6, '/uploads/man/06_tissot_t014/t3.jpg', 3),
(24, 6, '/uploads/man/06_tissot_t014/t4.jpg', 4),
(25, 7, '/uploads/man/07_tissot-t008-414/t1.jpg', 1),
(26, 7, '/uploads/man/07_tissot-t008-414/t2.jpg', 2),
(27, 7, '/uploads/man/07_tissot-t008-414/t3.jpg', 3),
(28, 7, '/uploads/man/07_tissot-t008-414/t4.jpg', 4),
(29, 8, '/uploads/man/08_tissot-t91-1/t1.jpg', 1),
(30, 8, '/uploads/man/08_tissot-t91-1/t2.jpg', 2),
(31, 8, '/uploads/man/08_tissot-t91-1/t3.jpg', 3),
(32, 8, '/uploads/man/08_tissot-t91-1/t4.jpg', 4),
(33, 9, '/uploads/man/09_edox_10305_3nv/e1.jpg', 1),
(34, 9, '/uploads/man/09_edox_10305_3nv/e2.jpg', 2),
(35, 9, '/uploads/man/09_edox_10305_3nv/e3.jpg', 3),
(36, 9, '/uploads/man/09_edox_10305_3nv/e4.jpg', 4),
(37, 10, '/uploads/man/10_edox-83007-3/e1.jpg', 1),
(38, 10, '/uploads/man/10_edox-83007-3/e2.jpg', 2),
(39, 10, '/uploads/man/10_edox-83007-3/e3.jpg', 3),
(40, 10, '/uploads/man/10_edox-83007-3/e4.jpg', 4),
(41, 11, '/uploads/man/11_edox-64009/e1.jpg', 1),
(42, 11, '/uploads/man/11_edox-64009/e2.jpg', 2),
(43, 11, '/uploads/man/11_edox-64009/e3.jpg', 3),
(44, 11, '/uploads/man/11_edox-64009/e4.jpg', 4),
(45, 12, '/uploads/man/12_edox-85014/e1.jpg', 1),
(46, 12, '/uploads/man/12_edox-85014/e2.jpg', 2),
(47, 12, '/uploads/man/12_edox-85014/e3.jpg', 3),
(48, 12, '/uploads/man/12_edox-85014/e4.jpg', 4),
(49, 13, '/uploads/man/13_raymond-weil-2839/r1.jpg', 1),
(50, 13, '/uploads/man/13_raymond-weil-2839/r2.jpg', 2),
(51, 13, '/uploads/man/13_raymond-weil-2839/r3.jpg', 3),
(52, 13, '/uploads/man/13_raymond-weil-2839/r4.jpg', 4),
(53, 14, '/uploads/man/14_raymond-weil-2846/r1.jpg', 1),
(54, 14, '/uploads/man/14_raymond-weil-2846/r2.jpg', 2),
(55, 14, '/uploads/man/14_raymond-weil-2846/r3.jpg', 3),
(56, 14, '/uploads/man/14_raymond-weil-2846/r4.jpg', 4),
(57, 15, '/uploads/man/15_victorinox_swiss_army_v241651/v1.jpg', 1),
(58, 15, '/uploads/man/15_victorinox_swiss_army_v241651/v2.jpg', 2),
(59, 15, '/uploads/man/15_victorinox_swiss_army_v241651/v3.jpg', 3),
(60, 15, '/uploads/man/15_victorinox_swiss_army_v241651/v4.jpg', 4),
(61, 16, '/uploads/man/16_victorinox_swiss_army_v241652/v1.jpg', 1),
(62, 16, '/uploads/man/16_victorinox_swiss_army_v241652/v2.jpg', 2),
(63, 16, '/uploads/man/16_victorinox_swiss_army_v241652/v3.jpg', 3),
(64, 16, '/uploads/man/16_victorinox_swiss_army_v241652/v4.jpg', 4),
(65, 17, '/uploads/man/17_victorinox_swiss_army_v241424/v1.jpg', 1),
(66, 17, '/uploads/man/17_victorinox_swiss_army_v241424/v2.jpg', 2),
(67, 17, '/uploads/man/17_victorinox_swiss_army_v241424/v3.jpg', 3),
(68, 17, '/uploads/man/17_victorinox_swiss_army_v241424/v4.jpg', 4),
(69, 18, '/uploads/man/18_victorinox_swiss_army_v241562/v1.jpg', 1),
(70, 18, '/uploads/man/18_victorinox_swiss_army_v241562/v2.jpg', 2),
(71, 18, '/uploads/man/18_victorinox_swiss_army_v241562/v3.jpg', 3),
(72, 18, '/uploads/man/18_victorinox_swiss_army_v241562/v4.jpg', 4),
(73, 19, '/uploads/man/19_maurice-lacroix-lc1008/m1.jpg', 1),
(74, 19, '/uploads/man/19_maurice-lacroix-lc1008/m2.jpg', 2),
(75, 19, '/uploads/man/19_maurice-lacroix-lc1008/m3.jpg', 3),
(76, 19, '/uploads/man/19_maurice-lacroix-lc1008/m4.jpg', 4),
(77, 20, '/uploads/man/20_maurice-lacroix-pt6148/m1.jpg', 1),
(78, 20, '/uploads/man/20_maurice-lacroix-pt6148/m2.jpg', 2),
(79, 20, '/uploads/man/20_maurice-lacroix-pt6148/m3.jpg', 3),
(80, 20, '/uploads/man/20_maurice-lacroix-pt6148/m4.jpg', 4),
(81, 21, '/uploads/woman/21_cimier_1708/c1.jpg', 1),
(82, 21, '/uploads/woman/21_cimier_1708/c2.jpg', 2),
(83, 21, '/uploads/woman/21_cimier_1708/c3.jpg', 3),
(84, 21, '/uploads/woman/21_cimier_1708/c4.jpg', 4);

-- --------------------------------------------------------

--
-- Структура таблиці `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `main_path_to_file` varchar(255) NOT NULL,
  `brand_name` varchar(255) NOT NULL,
  `model` varchar(255) NOT NULL,
  `product_code` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `mail` varchar(49) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `is_in` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `product`
--

INSERT INTO `product` (`id`, `main_path_to_file`, `brand_name`, `model`, `product_code`, `price`, `currency`, `status`, `mail`, `sort_order`, `is_in`) VALUES
(1, '/uploads/man/01_tissot_t41/01_tissot_t41.jpg', 'Tissot', 'T41.1.423.33', 14480, 14429, 'UAH', 1, 'man', 1, 1),
(2, '/uploads/man/02_tissot_t035/02_tissot_t035.jpg', 'Tissot', 'T035.617.16.031.00', 14468, 13919, 'UAH', 1, 'man', 10, 0),
(3, '/uploads/man/03_tissot_t055/03_tissot_t055.jpg', 'Tissot', 'T055.417.16.047.00', 44803, 13289, 'UAH', 1, 'man', 7, 0),
(4, '/uploads/man/04_tissot_t055_427/04_tissot_t055_427.jpg', 'Tissot', 'T055.417.16.047.00', 48735, 27529, 'UAH', 2, 'man', 8, 1),
(5, '/uploads/man/05_tissot_t068_427/05_tissot_t068_427.jpg', 'Tissot', 'T068.427.16.011.00', 14478, 22749, 'UAH', 2, 'man', 16, 0),
(6, '/uploads/man/06_tissot_t014/06_tissot_t014.jpg', 'Tissot', 'T014.427.11.051.00', 18304, 25839, 'UAH', 2, 'man', 12, 1),
(7, '/uploads/man/07_tissot-t008-414/07_tissot-t008-414.jpg', 'Tissot', 'T008.414.16.201', 44772, 39934, 'UAH', 2, 'man', 6, 1),
(8, '/uploads/man/08_tissot-t91-1/08_tissot-t91-1.jpg', 'Tissot', 'T91.1.427.81', 44830, 40325, 'UAH', 3, 'man', 19, 1),
(9, '/uploads/man/09_edox_10305_3nv/09_edox_10305_3nv.jpg', 'Edox', '10305 3NV NV', 18774, 30125, 'UAH', 3, 'man', 20, 0),
(10, '/uploads/man/10_edox-83007-3/10_edox-83007-3.jpg', 'Edox', '83007 3 AIN', 2457, 31000, 'UAH', 3, 'man', 5, 1),
(11, '/uploads/man/11_edox-64009/11_edox-64009.jpg', 'Edox', '64009 3 NIN2', 12291, 39750, 'UAH', 3, 'man', 9, 1),
(12, '/uploads/man/12_edox-85014/12_edox-85014.jpg', 'Edox', '85014 37R GIR', 12227, 36000, 'UAH', 3, 'man', 17, 1),
(13, '/uploads/man/13_raymond-weil-2839/13_raymond-weil-2839.jpg', 'Raymond Weil', '839-STC-00659', 45437, 35240, 'UAH', 3, 'man', 4, 1),
(14, '/uploads/man/14_raymond-weil-2846/14_raymond-weil-2846.jpg', 'Raymond Weil', '2846-STC-00209', 2846, 35240, 'UAH', 3, 'man', 15, 1),
(15, '/uploads/man/15_victorinox_swiss_army_v241651/15_victorinox_swiss_army_v241651.jpg', 'Victorinox Swiss Army', 'V241651', 36079, 20240, 'UAH', 2, 'man', 3, 1),
(16, '/uploads/man/16_victorinox_swiss_army_v241652/16_victorinox_swiss_army_v241652.jpg', 'Victorinox Swiss Army', 'V241652', 36080, 22522, 'UAH', 2, 'man', 18, 1),
(17, '/uploads/man/17_victorinox_swiss_army_v241424/17_victorinox_swiss_army_v241424.jpg', 'Victorinox Swiss Army', 'V241424', 35982, 31417, 'UAH', 2, 'man', 11, 1),
(18, '/uploads/man/18_victorinox_swiss_army_v241562/18_victorinox_swiss_army_v241562.jpg', 'Victorinox Swiss Army', 'V241562', 36040, 32630, 'UAH', 3, 'man', 13, 1),
(19, '/uploads/man/19_maurice-lacroix-lc1008/19_maurice-lacroix-lc1008.jpg', 'Maurice Lacroix', 'LC1008-PVY11-130', 45911, 35480, 'UAH', 3, 'man', 2, 1),
(20, '/uploads/man/20_maurice-lacroix-pt6148/20_maurice-lacroix-pt6148.jpg', 'Maurice Lacroix', 'PT6148-SS002-230', 46008, 32865, 'UAH', 3, 'man', 14, 1),
(21, '/uploads/woman/21_cimier_1708/21_cimier_1708.jpg', 'Cimier', '1708-SZ611', 46788, 28387, 'UAH', 3, 'woman', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблиці `product_option`
--

CREATE TABLE `product_option` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `product_value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `product_option`
--

INSERT INTO `product_option` (`id`, `product_id`, `option_id`, `product_value`) VALUES
(1, 1, 1, 'Switzerland'),
(2, 1, 2, 'An arrow'),
(3, 1, 3, 'Mechanical with automatic winding'),
(4, 1, 4, 'Male'),
(5, 1, 5, 'Steel'),
(6, 1, 6, '30'),
(7, 1, 7, 'Sapphire'),
(8, 1, 8, 'Leather'),
(9, 1, 9, '39'),
(10, 1, 10, '68'),
(11, 1, 11, '39'),
(12, 1, 12, '9'),
(13, 1, 13, '+'),
(14, 1, 14, '-'),
(15, 1, 15, '-'),
(16, 2, 1, 'Switzerland'),
(17, 2, 2, 'An arrow'),
(18, 2, 3, 'Quartz'),
(19, 2, 4, 'Male'),
(20, 2, 5, 'Steel'),
(21, 2, 6, '100'),
(22, 2, 7, 'Sapphire'),
(23, 2, 8, 'Leather'),
(24, 2, 9, '41'),
(25, 2, 10, '83'),
(26, 2, 11, '41'),
(27, 2, 12, '10.8'),
(28, 2, 13, '+'),
(29, 2, 14, '+'),
(30, 2, 15, '+'),
(31, 3, 1, 'Switzerland'),
(32, 3, 2, 'An arrow'),
(33, 3, 3, 'Quartz'),
(34, 3, 4, 'Male'),
(35, 3, 5, 'Steel'),
(36, 3, 6, '200'),
(37, 3, 7, 'Sapphire'),
(38, 3, 8, 'Leather'),
(39, 3, 13, '+'),
(40, 3, 14, '+'),
(41, 3, 15, '-'),
(42, 4, 1, 'Switzerland'),
(43, 4, 2, 'An arrow'),
(44, 4, 3, 'Mechanical with automatic winding'),
(45, 4, 4, 'Male'),
(46, 4, 5, 'Steel'),
(47, 4, 6, '200'),
(48, 4, 7, 'Sapphire'),
(49, 4, 8, 'Caoutchouc'),
(50, 4, 9, '44'),
(51, 4, 10, '128'),
(52, 4, 11, '43'),
(53, 4, 12, '16'),
(54, 4, 13, '+'),
(55, 4, 14, '+'),
(56, 4, 15, '+'),
(57, 5, 1, 'Switzerland'),
(58, 5, 2, 'An arrow'),
(59, 5, 3, 'Mechanical with automatic winding'),
(60, 5, 4, 'Male'),
(61, 5, 5, 'Steel'),
(62, 5, 6, '30'),
(63, 5, 7, 'Sapphire'),
(64, 5, 8, 'Leather'),
(65, 5, 9, '43'),
(66, 5, 10, '100'),
(67, 5, 11, '43'),
(68, 5, 12, '14'),
(69, 5, 13, '+'),
(70, 5, 14, '+'),
(71, 5, 15, '-'),
(72, 6, 1, 'Switzerland'),
(73, 6, 2, 'An arrow'),
(74, 6, 3, 'Mechanical with automatic winding'),
(75, 6, 4, 'Male'),
(76, 6, 5, 'Steel'),
(77, 6, 6, '200'),
(78, 6, 7, 'Sapphire'),
(79, 6, 8, 'Steel'),
(80, 6, 9, '43.6'),
(81, 6, 10, '176'),
(82, 6, 11, '42'),
(83, 6, 12, '15.8'),
(84, 6, 13, '+'),
(85, 6, 14, '+'),
(86, 6, 15, '-'),
(87, 7, 1, 'Switzerland'),
(88, 7, 2, 'An arrow'),
(89, 7, 3, 'Mechanistic'),
(90, 7, 4, 'Male'),
(91, 7, 5, 'Steel'),
(92, 7, 6, '100'),
(93, 7, 7, 'Sapphire'),
(94, 7, 8, 'Leather'),
(95, 7, 9, '42.05'),
(96, 7, 10, '39.8'),
(97, 7, 12, '14.29'),
(98, 7, 13, '+'),
(99, 7, 14, '+'),
(100, 7, 15, '-'),
(101, 8, 1, 'Switzerland'),
(102, 8, 2, 'An arrow'),
(103, 8, 3, 'Mechanistic'),
(104, 8, 4, 'Male'),
(105, 8, 5, 'Steel'),
(106, 8, 6, '100'),
(107, 8, 7, 'Sapphire'),
(108, 8, 8, 'Leather'),
(109, 8, 13, '+'),
(110, 8, 14, '+'),
(111, 8, 15, '-'),
(112, 9, 1, 'Switzerland'),
(113, 9, 2, 'An arrow'),
(114, 9, 3, 'Quartz'),
(115, 9, 4, 'Male'),
(116, 9, 5, 'Steel+PVD'),
(117, 9, 6, '100'),
(118, 9, 7, 'Sapphire'),
(119, 9, 8, 'Caoutchouc'),
(120, 9, 9, '45'),
(121, 9, 10, '127'),
(122, 9, 11, '45'),
(123, 9, 12, '122'),
(124, 9, 13, '+'),
(125, 9, 14, '+'),
(126, 9, 15, '+'),
(127, 10, 1, 'Switzerland'),
(128, 10, 2, 'An arrow'),
(129, 10, 3, 'Mechanical with automatic winding'),
(130, 10, 4, 'Male'),
(131, 10, 5, 'Steel'),
(132, 10, 6, '50'),
(133, 10, 7, 'Sapphire'),
(134, 10, 8, 'Leather'),
(135, 10, 13, '+'),
(136, 10, 14, '-'),
(137, 10, 15, '-'),
(138, 11, 1, 'Switzerland'),
(139, 11, 2, 'An arrow'),
(140, 11, 3, 'Quartz'),
(141, 11, 4, 'Male'),
(142, 11, 5, 'Steel'),
(143, 11, 6, '100'),
(144, 11, 7, 'Sapphire'),
(145, 11, 8, 'Steel'),
(146, 11, 13, '+'),
(147, 11, 14, '-'),
(148, 11, 15, '-'),
(149, 12, 1, 'Switzerland'),
(150, 12, 2, 'An arrow'),
(151, 12, 3, 'Mechanical with automatic winding'),
(152, 12, 4, 'Male'),
(153, 12, 5, 'Steel'),
(154, 12, 6, '50'),
(155, 12, 7, 'Sapphire'),
(156, 12, 8, 'Leather'),
(157, 12, 9, '42'),
(158, 12, 11, '42'),
(159, 12, 12, '11.5'),
(160, 12, 13, '+'),
(161, 12, 14, '-'),
(162, 12, 15, '-'),
(163, 13, 1, 'Switzerland'),
(164, 13, 2, 'An arrow'),
(165, 13, 3, 'Mechanistic'),
(166, 13, 4, 'Male'),
(167, 13, 5, 'Steel'),
(168, 13, 6, '50'),
(169, 13, 7, 'Sapphire'),
(170, 13, 8, 'Leather'),
(171, 13, 9, '45.6'),
(172, 13, 11, '43'),
(173, 13, 12, '9.5'),
(174, 13, 13, '-'),
(175, 13, 14, '-'),
(176, 13, 15, '-'),
(177, 14, 1, 'Switzerland'),
(178, 14, 2, 'An arrow'),
(179, 14, 3, 'Mechanistic'),
(180, 14, 4, 'Male'),
(181, 14, 5, 'Steel'),
(182, 14, 6, '50'),
(183, 14, 7, 'Sapphire'),
(184, 14, 8, 'Leather'),
(185, 14, 9, '39.5'),
(186, 14, 11, '45.6'),
(187, 14, 12, '10.45'),
(188, 14, 13, '+'),
(189, 14, 14, '-'),
(190, 14, 15, '-'),
(191, 15, 1, 'Switzerland'),
(192, 15, 2, 'An arrow'),
(193, 15, 3, 'Quartz'),
(194, 15, 4, 'Male'),
(195, 15, 5, 'Steel+PVD'),
(196, 15, 6, '100'),
(197, 15, 7, 'Sapphire'),
(198, 15, 8, 'Leather'),
(199, 15, 9, '45'),
(200, 15, 11, '45'),
(201, 15, 12, '12'),
(202, 15, 13, '+'),
(203, 15, 14, '+'),
(204, 15, 15, '+'),
(205, 16, 1, 'Switzerland'),
(206, 16, 2, 'An arrow'),
(207, 16, 3, 'Quartz'),
(208, 16, 4, 'Male'),
(209, 16, 5, 'Steel+PVD'),
(210, 16, 6, '100'),
(211, 16, 7, 'Sapphire'),
(212, 16, 8, 'Steel'),
(213, 16, 9, '45'),
(214, 16, 11, '45'),
(215, 16, 12, '12'),
(216, 16, 13, '+'),
(217, 16, 14, '+'),
(218, 16, 15, '+'),
(219, 17, 1, 'Switzerland'),
(220, 17, 2, 'An arrow'),
(221, 17, 3, 'Quartz'),
(222, 17, 4, 'Male'),
(223, 17, 5, 'Steel+PVD'),
(224, 17, 6, '500'),
(225, 17, 7, 'Sapphire'),
(226, 17, 8, 'Steel'),
(227, 17, 9, '43'),
(228, 17, 11, '43'),
(229, 17, 12, '14'),
(230, 17, 13, '+'),
(231, 17, 14, '+'),
(232, 17, 15, '-'),
(233, 18, 1, 'Switzerland'),
(234, 18, 2, 'An arrow'),
(235, 18, 3, 'Mechanical with automatic winding'),
(236, 18, 4, 'Male'),
(237, 18, 5, 'Steel+PVD'),
(238, 18, 6, '500'),
(239, 18, 7, 'Sapphire'),
(240, 18, 8, 'Caoutchouc'),
(241, 18, 9, '43'),
(242, 18, 11, '43'),
(243, 18, 12, '14'),
(244, 18, 13, '+'),
(245, 18, 14, '-'),
(246, 18, 15, '-'),
(247, 19, 1, 'Switzerland'),
(248, 19, 2, 'An arrow'),
(249, 19, 3, 'Quartz'),
(250, 19, 4, 'Male'),
(251, 19, 5, 'Steel'),
(252, 19, 6, '30'),
(253, 19, 7, 'Sapphire'),
(254, 19, 8, 'Leather'),
(255, 19, 13, '+'),
(256, 19, 14, '-'),
(257, 19, 15, '-'),
(258, 20, 1, 'Switzerland'),
(259, 20, 2, 'An arrow'),
(260, 20, 3, 'Mechanistic'),
(261, 20, 4, 'Male'),
(262, 20, 5, 'Steel'),
(263, 20, 6, '30'),
(264, 20, 7, 'Sapphire'),
(265, 20, 8, 'Steel'),
(266, 20, 13, '+'),
(267, 20, 14, '-'),
(268, 20, 15, '-'),
(269, 21, 1, 'Switzerland'),
(270, 21, 2, 'An arrow'),
(271, 21, 3, 'Quartz'),
(272, 21, 4, 'Ladies'),
(273, 21, 5, 'Steel+PVD'),
(274, 21, 6, '30'),
(275, 21, 7, 'Sapphire'),
(276, 21, 8, 'Caoutchouc'),
(277, 21, 9, '34'),
(278, 21, 11, '36'),
(279, 21, 13, '+'),
(280, 21, 14, '-'),
(281, 21, 15, '-');

-- --------------------------------------------------------

--
-- Структура таблиці `product_order`
--

CREATE TABLE `product_order` (
  `id` int(11) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `quantity` int(11) NOT NULL,
  `sum` float NOT NULL,
  `status` enum('0','1') DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `product_order`
--

INSERT INTO `product_order` (`id`, `created_at`, `updated_at`, `quantity`, `sum`, `status`, `name`, `email`, `phone`, `address`) VALUES
(1, '2017-01-15 21:59:41', '2017-01-15 21:59:41', 2, 43414, '1', 'Vadim', 'vadim@gmail.com', '0978654343', 'val43'),
(2, '2017-01-16 23:11:44', '2017-01-16 23:11:44', 7, 380320, NULL, 'Maaria', '2@2.com', '0978654567', 'kiev'),
(3, '2017-01-16 23:34:23', '2017-01-16 23:34:23', 5, 113745, NULL, 'Ihor', 'werb@mail.com', '0983212459', 'oytr'),
(4, '2017-02-11 14:34:42', '2017-02-11 14:34:42', 1, 14429, NULL, 'IvanZack', 'vadim123@gmail.com', '0987653342', 'lenina 45');

-- --------------------------------------------------------

--
-- Структура таблиці `session`
--

CREATE TABLE `session` (
  `id` char(40) NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `data` longblob
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `session`
--

INSERT INTO `session` (`id`, `expire`, `data`) VALUES
('1o865rh8oagqbc0lumh6dgo6l2', 1503750134, 0x5f5f666c6173687c613a303a7b7d69647c733a323a223139223b636172747c613a303a7b7d636172742e7175616e746974797c693a303b636172742e73756d7c693a303b);

-- --------------------------------------------------------

--
-- Структура таблиці `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `role` smallint(6) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `role`) VALUES
(2, 'Vadim', 'lNoeZCG75TWQ0Ktw0KENiYvOchc4ReT3', '$2y$13$7fykot.rW8V3yzel3HxUxepbMIwbvOKMwmPPx/Alt7AfY2rc74ZLK', NULL, 'vadymbondarenko91@gmail.com', 10, 1483639092, 1483639092, 10),
(6, 'Vetal', 'DORTXk6bqY9zucg8XOK8Jb4ksT2NCNE1', '$2y$13$633e/608A9ejeyp8xXw5xOIxaSJWT5yyp0jsSYY/37Y6JS6xL74iS', NULL, '0001ral@ukr.net', 10, 1502983198, 1502983198, 10);

-- --------------------------------------------------------

--
-- Структура таблиці `user_login`
--

CREATE TABLE `user_login` (
  `id` int(11) UNSIGNED NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `auth_key` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `compare`
--
ALTER TABLE `compare`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `description`
--
ALTER TABLE `description`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Індекси таблиці `option`
--
ALTER TABLE `option`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `order_item`
--
ALTER TABLE `order_item`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `photo`
--
ALTER TABLE `photo`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `product_option`
--
ALTER TABLE `product_option`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `product_order`
--
ALTER TABLE `product_order`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- Індекси таблиці `user_login`
--
ALTER TABLE `user_login`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблиці `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT для таблиці `compare`
--
ALTER TABLE `compare`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблиці `description`
--
ALTER TABLE `description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT для таблиці `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=150;
--
-- AUTO_INCREMENT для таблиці `option`
--
ALTER TABLE `option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблиці `order_item`
--
ALTER TABLE `order_item`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблиці `photo`
--
ALTER TABLE `photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;
--
-- AUTO_INCREMENT для таблиці `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT для таблиці `product_option`
--
ALTER TABLE `product_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=282;
--
-- AUTO_INCREMENT для таблиці `product_order`
--
ALTER TABLE `product_order`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблиці `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблиці `user_login`
--
ALTER TABLE `user_login`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
