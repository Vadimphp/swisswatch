<?php

namespace frontend\models;

use yii\db\ActiveRecord;
/**
 * This is the model class for table "photo".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $path_to_file
 * @property integer $sort_order
 */
class Photo extends ActiveRecord
{
    //для загрузки картинок с админки
    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'photo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'path_to_file', 'sort_order'], 'required'],
            [['product_id', 'sort_order'], 'integer'],
            [['path_to_file'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'path_to_file' => 'Path To File',
            'sort_order' => 'Sort Order',
        ];
    }
    
    
}
