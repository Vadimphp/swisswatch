<?php

namespace frontend\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $name
 * @property string $text
 */
class Comment extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'name', 'text'], 'required'],
            [['product_id'], 'integer'],
            [['rating'], 'integer'],
            [['name'], 'string', 'max' => 49],
            [['text'], 'string', 'min' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'name' => 'Name',
            'text' => 'Text',
            'rating' => 'Rating',
        ];
    }
}
