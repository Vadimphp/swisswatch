<?php

namespace frontend\models;

use Yii;
use yii\web\NotFoundHttpException;
use yii\db\ActiveRecord;
use yii\helpers\Url;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $main_path_to_file
 * @property string $brand_name
 * @property string $model
 * @property string $product_code
 * @property integer $price
 * @property string $currency
 * @property integer $status
 * @property integer $mail
 * @property integer $sort_order
 * @property integer $is_in
 */
class Product extends ActiveRecord
{
    
    // change pictures on backend
    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }
    
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image', 'main_path_to_file', 'brand_name', 'model', 'product_code', 'price', 'currency', 'status', 'mail', 'sort_order', 'is_in'], 'required'],
            [['price', 'status', 'sort_order', 'is_in', 'product_code'], 'integer'],
            [['image', 'main_path_to_file', 'brand_name', 'model', 'currency'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'png, jpg'],
            [['mail'], 'string', 'max' => 49],
            [['gallery'], 'file', 'extensions' => 'png, jpg', 'maxFiles' => 4],//перенести в галерею
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'main_path_to_file' => 'Main Path To File',
            'image' => 'Photo',
            'brand_name' => 'Brand Name',
            'model' => 'Model',
            'image' => 'Photo',
            'gallery' => 'Gallery',//перенести в галерею
            'product_code' =>'Product_Code',
            'price' => 'Price',
            'currency' => 'Currency',
            'status' => 'Status',
            'mail' => 'Mail',
            'sort_order' => 'Sort Order',
            'is_in' => 'Is In',
            
        ];
    }
    
    /**
     * Check whether product is in stock
     * @return boolean
     */
    public function isInstock()
    {
        return ($this->is_in > 0);
    }   
   
    public function getOptionsfrom () 
    {
        $className = ProductOption::className();
        $link = ['product_id' => 'id'];
        
        return $this->hasMany($className, $link)->limit(15);
    }
    
    
    public function choseMail($mail)
    {       
        if ($mail == womans) { 
            $conditional['mail'] = 'woman';
            return $conditional;            
        } elseif ($mail == mans) {
            $conditional['mail'] = 'man';
            return $conditional;          
        } elseif ($mail == kids) {
            $conditional['mail'] = 'kids';
            return $conditional;     
        } else { 
            throw new NotFoundHttpException('The requested page does not exist.'); 
        }
    }
    
    public function filter ($conditional, $getBrand) 
    {
        
        if($getBrand) {
            $checkBrand = "'".implode("', '", $getBrand)."'";
        } 
        $startPrice = (int)Yii::$app->request->get('minCost');
        $endPrice = (int)Yii::$app->request->get('maxCost');
        
        if(!empty($checkBrand) || !empty($endPrice)) {
            if (!empty($checkBrand)) {
               $queryBrand = " brand_name IN($checkBrand)";              
            }
            if (!empty($endPrice)) {
               $queryPrice = " price BETWEEN $startPrice AND $endPrice"; 
             
            } 
        }

        $query = Product::find()->where($queryBrand)->andWhere($queryPrice)->andWhere($conditional); 
        return  $query;
      
    }
    
     public function price() 
    {
         $startPrice = (int)Yii::$app->request->get('minCost');
         
         if ($startPrice >= 0 AND $startPrice <= 60000) {
            $nameMin = $startPrice;
            return $nameMin;
        } 
            $nameMin = "0";
            return $nameMin;
    }
    
}
