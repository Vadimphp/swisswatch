<?php

namespace frontend\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "compare".
 *
 * @property integer $id
 * @property integer $product_id
 */
class Compare extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'compare';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id'], 'required'],
            [['product_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
        ];
    }
    
    
}
