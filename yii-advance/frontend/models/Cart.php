<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;
/**
 * Description of Cart
 *
 * @author Вадим
 */
class Cart extends ActiveRecord 
{
    
    // pictures change on backend
    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }
    
     public function addToCart($product, $quantity = 1)
    {
        $mainImg = $product->getImage(); // get img from backend
        $session = Yii::$app->session;
        //$session['cart'] = new \ArrayObject;
        $cart = $session['cart'];
        
        if(isset($cart[$product->id]) && $session->open()) {
            $cart[$product->id] = [
                'quantity' => $quantity
            ];
        } else {
            
             $cart[$product->id] = [
                'quantity' => $quantity,
                'brand_name' => $product->brand_name,
                'price' => $product->price,
                'img' => $mainImg->getUrl() // get img from backend
             ];
        }
        $session['cart'] = $cart;

        $session['cart.quantity'] = isset($session['cart.quantity']) 
                ? $session['cart.quantity'] + $quantity : $quantity;
        
        $session['cart.sum'] = isset($session['cart.sum']) 
                ? $session['cart.sum'] + $quantity * $product->price : $quantity * $product->price;
    }
    
    public function recalculation($id) 
    {      
        $session = Yii::$app->session;
        
        $carts = $session['cart'];
        
        if(!isset($carts[$id]) && $session->open()) {
            return false;
        }
               
        $quantityDecrease = $carts[$id]['quantity'];
        $sumDecrease = $quantityDecrease * $carts[$id]['price'];
        
        $session['cart.quantity'] -= $quantityDecrease;
        $session['cart.sum'] -= $sumDecrease;

        unset($carts[$id]);
        
        $session['cart'] = $carts;
        
    }

    public function sendMail($send_mail)
    {
        $session = Yii::$app->session;
        
        $post_mail = Yii::$app->mailer->compose('order', ['session' => $session]) // order it is php file
                ->setFrom(['vadymbondarenko91@gmail.com' => 'watch4life'])
                ->setTo($send_mail)
                ->setSubject('Order from site')
                ->send();
        
        return $post_mail;
    }
    
  }  
    
    
    
    
   
  
  
  
  
  

 

