<?php
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\helpers\Html;
use frontend\assets\CarouselAsset; /* @var $this yii\web\View one more metod to add concrete Asset*/

CarouselAsset::register($this);
/**
 *  @var WatchProduct 
 */
$this->title = ucfirst($mail);
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="fh5co-why-us" class="list-page">

    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-4">

                <div class="filtre_form">

                    <?php echo Html::beginForm(Url::to(), 'get', $options = ['id' => 'filterForm']); ?>

                    <ul class="checkbox">
                        <h3>Brands</h3>                     
                        <?php foreach ($filter as $brand): ?>
                           
                            <?php 
                                $checkedBrand = "";
                                 if ($getBrand) {
                                    if (in_array($brand->brand_name, $getBrand)){
                                        $checkedBrand = "checked";                                      
                                    }
                                }
                            ?>
                        <li>
                          <?= Html::checkbox(
                                'brand[]',
                                $checked = $checkedBrand,                               
                                $options = [
                                    'id' => "check" . $brand->id,
                                    'value' => $brand->brand_name,                              
                                  ]
                                ); ?>
                            <label for="check<?php echo $brand->id; ?>"><?php echo $brand->brand_name; ?></label>
                         </li>
                        <?php endforeach; ?>    
                    </ul>
                    
                    
                    <div class="formCost ">
                        <label for="minCost">Price: From
                            <?= Html::input($type, $name = 'minCost', $value = $nameMin, $options = ['id' => 'minCost']); ?>
                        </label> 
                        <label for="maxCost">To
                            <?= Html::input($type, $name = 'maxCost', $value = "60000", $options = ['id' => 'maxCost']); ?>
                        </label>
                    </div>
                    <div class="sliderCont">
                        <div id="slider"></div>
                    </div>

                    <div class="form-group">
                        <?= Html::submitButton('Find', ['class' => 'btn btn-primary']) ?>
                    </div>

                    <?php echo Html::endForm() ?>
                   
		</div>
            </div>
                     
            
            
            <div class="col-md-9 col-sm-8">
                <?php Pjax::begin(['id' => 'item']); ?>                   
                <?php
                    echo ListView::widget([
                        'dataProvider' => $dataProvider,                       
                        'layout' => "{pager}\n{items}\n{summary}",
                        'itemView' => '_list',
                        'options' => [
                            'tag' => 'div',
                            'class' => 'text-center',
                            'id' => 'pager', // не заполнял
                        ],
                    ]);
                ?>
                <?php Pjax::end(); ?>
            </div>


        </div>
   </div>

</div>































