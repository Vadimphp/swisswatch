<?php

use frontend\models\Product;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\web\DbSession;
use frontend\assets\CarouselAsset; /* @var $this yii\web\View one more metod to add concrete Asset */

CarouselAsset::register($this); // add jquery.js to header
/**
 *  @var Product, show product by id
 * 
 */
/* @var $gallery show items from backend */
/* @var $product Product model  */
/* @var $item show characteristics, parameters of watch */
/* @var $comment send comment for product to data base and get it on site with ajax*/


$this->title = $product->model;  
$this->params['breadcrumbs'][] = ['label' => ucfirst($product->mail), 'url' => Url::to(['watch/mans'])];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
    $gallery = $product->getImages();
?>


<section class="watch-page">

    <div id="watch_carousel"  >
        <div class="container">
            <div class="row">
                <div class="col-md-6">

                    <div class="flexslider ">
                        <ul class="slides">

                            <?php if ($gallery[0]['urlAlias'] !== 'placeHolder') : ?>
                                <?php $gall = array_slice($gallery, 1); ?>
                                <?php foreach ($gall as $img): ?>

                                    <li data-thumb="<?php echo $img->getUrl('450x300'); ?>" 
                                        style="background-image: url(<?php echo $img->getUrl(); ?>);" >
                                    </li>

                                <?php endforeach; ?>
                            <?php endif; ?>

                        </ul>
                    </div>
                </div>

                <div class="col-md-6">

                    <div class="inform">

                        <h3><?php echo $product->brand_name; ?></h3>

                        <span><?php echo substr($product->model, 0, 11); ?></span>

                        <h4 class="money"><?php echo $product->price; ?>
                            <span><?php echo $product['currency']; ?></span>
                        </h4>

                        <?php if ($product->isInStock()): ?>
                        <h4 class="text-success">In Stock</h4> 
                        <?php else: ?>
                            <h4 class="text-danger">Out of stock</h4> 
                        <?php endif; ?>


                        <p><a href="<?php echo Url::to(['cart/add', 'id' => $product->id,]); ?>" 
                              data-id="<?= $product->id ?>" class="btn add-to-cart btn-primary btn-outline with-arrow">
                              Add to Cart<i class="icon-arrow-right"></i>
                            </a>
                        </p>


                        <div class="number">
                            <div class="minus qnt">-</div>
                            <input id="quantity"  type="text" value="1" size="1"/>
                            <div class="plus qnt">+</div>
                        </div>

                    </div>

                </div>
                
            </div>
        </div>
    </div>
    
</section>

<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8">

            <div class="content">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-tabs--ys1" role="tablist">
                    <li class="active"><a href="#Tab1" role="tab" data-toggle="tab" class="text-uppercase text-purple">DESCRIPTION</a></li>
                    <li><a href="#Tab2" role="tab" data-toggle="tab" class="text-uppercase text-purple">Comment</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content tab-content--ys nav-stacked">
                    <div role="tabpanel" class="tab-pane active" id="Tab1">
                        <div class="divider divider--md"></div>

                        <table class="table table-params">
                            <tbody>
                                <?php /* foreach ($product->getOptionsfrom() as $option): ?>
                                  <tr>
                                  <td class="text-right"><span class="color"><?php echo $option['option_name']; ?></span></td>
                                  <td><?php echo $option['product_value']; ?></td>
                                  </tr>
                                  <?php endforeach; */ ?>

                                <?php foreach ($product->optionsfrom as $item): ?>
                                    <tr>
                                        <td width="40%" class="text-left"><span class="text-purple">
                                                <?php echo $item->option_id; ?> | <?php echo $item->getName(); ?></span>
                                        </td>
                                        <td class="text-left">
                                            <?php echo $item->product_value; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                            </tbody>
                        </table>

                    </div>


                    <div role="tabpanel" class="tab-pane" id="Tab2">

                        <?php Pjax::begin(['id' => 'comment']); ?>

                        <?php if (!empty($commentsList) && is_array($commentsList)): ?>
                            <?php foreach ($commentsList as $comment): ?>
                        <strong><?php echo $comment->name; ?></strong><br>
                                <?php echo $comment->text; ?>
                                <hr>
                            <?php endforeach; ?>
                        <?php endif; ?>

                        <?php Pjax::end(); ?>

                        <?php
                            $form_comment = ActiveForm::begin([
                                'id' => 'commentForm',
                                'enableAjaxValidation' => true,
                                'validationUrl' => Url::to('validate-comment'),
                            ])
                        ?>

                        <?php echo $form_comment->field($commentModel, 'product_id')->hiddenInput()->label(false); ?>

                        <?php echo $form_comment->field($commentModel, 'name'); ?>

                        <?php echo $form_comment->field($commentModel, 'text')->textarea(); ?>

                        <?php echo yii\helpers\Html::submitButton('Ok', ['class' => 'btn btn-primary btn--ys text-uppercase']); ?>

                        <?php ActiveForm::end(); ?>    

                    </div>


                </div>



            </div>




        </div>
    </div>
</div>


<?php include_once 'custom_js.php'; ?>
