<?php

use frontend\models\Product;

use frontend\assets\CarouselAsset; /* @var $this yii\web\View one more metod to add concrete Asset */

CarouselAsset::register($this); // add jquery.js to header
/**
 *  @var WatchProduct, show product by id
 * 
 */

/* @var $product Product */
?>
<div id="fh5co-why-us" class="list-page ">
    <div class="container">
        <div class="row">

            <?php foreach ($product as $product): ?>    
                <div class="col-md-3">
                    <div class="text-center item-block"> 
                        <span class="icon">
                            <img src="<?php echo $product->main_path_to_file; ?>" alt="" class="img-responsive">
                        </span>

                        <h3><?php echo $product->brand_name; ?></h3>

                        <span><?php echo substr($product->model, 0, 6); ?></span>

                        <h4><?php echo $product->price; ?>
                            <span><?php echo $product->currency; ?></span>
                        </h4>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>
    </div>
</div>