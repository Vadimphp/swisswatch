<?php

use yii\helpers\Url;
/**
 *  @var Product
 *  @var get item from backend and other parameters for one product and put it in dataProvider 
 */
?>

<div class="text-center item-block">

    <span class="icon">
        <img src="<?php echo $model->getImage()->getUrl(); ?>" alt="" class="img-responsive">
    </span>
				
    <h3><?php echo $model->brand_name; ?></h3>
                               
    <span><?php echo substr($model->model, 0, 11); ?></span>
                                
    <h4><?php echo $model->price; ?>
        <span><?php echo $model->currency; ?></span>
    </h4>
				
    <p>
        <a href="<?php echo Url::to(['product/view', 'id' => $model -> id, ]); ?>"
            class="btn btn-primary btn-outline with-arrow">Look at them<i class="icon-arrow-right"></i>
        </a>
    </p>
                                    
</div>
         