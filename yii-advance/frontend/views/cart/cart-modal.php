<?php

use yii\helpers\Html;
    
?>

<?php if (!empty($session['cart'])) : ?>

    <div class="table-responsive">
        
        <table class="table table-hover table-striped">
            
            <thead>
                <tr>
                    <th>Photo</th>
                    <th>Brand name</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th><span class="glyphicon glyphicon-remove" aria-hidden="false"></span></th>
                </tr>
            </thead>
            
            <tbody>
                
                <?php foreach ($session['cart'] as $id => $item) : ?>
                <tr>
                    <td><?= Html::img("{$item['img']}", ['alt' => $item['brand_name'], 'height' => 200]) ?></td>
                    <td><?= $item['brand_name'] ?></td>
                    <td><?= $item['quantity'] ?></td>
                    <td><?= $item['price'] ?></td>
                    <td><span data-id="<?= $id ?>" class="glyphicon glyphicon-remove text-danger del-item" aria-hidden="true"></span></td>                   
                </tr>
                <?php endforeach?>
                
                <tr>
                    <td colspan="4">In total:</td>
                    <td><?= $session['cart.quantity'] ?></td>
                </tr>
                
                <tr>
                    <td colspan="4">Amount:</td>
                    <td><?= $session['cart.sum'] ?></td>
                </tr>
                
            </tbody>
            
        </table>
        
    </div>

<?php else : ?>

    <h3>Cart is empty!</h3>
    
<?php endif; ?>



