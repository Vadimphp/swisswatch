<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use frontend\assets\CarouselAsset; 

CarouselAsset::register($this); // add jquery.js to header

?>

<div class="container">

    <?php if (!empty($session['cart'])) : ?>
    
        <div class="table-responsive" id="order">
            
            <table class="table table-hover table-striped">
                
                <thead>
                    <tr>
                        <th>Photo</th>
                        <th>Brand name</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Sum</th>
                        <th><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></th>
                    </tr>
                </thead>
                
                <tbody>
                    
                    <?php foreach ($session['cart'] as $id => $item) : ?>
                        <tr>
                            <td>
                                <a href="<?php echo Url::to(['product/view', 'id' => $id,]); ?>">
                                    <?= Html::img("{$item['img']}", ['alt' => $item['brand_name'], 'height' => 200]) ?>
                                </a>
                            </td>                     
                            <td><?= $item['brand_name'] ?></td>
                            <td><?= $item['quantity'] ?></td>
                            <td><?= $item['price'] ?></td>
                            <td><?= $item['price'] * $item['quantity'] ?></td>
                            <td><span data-id="<?= $id ?>" class="glyphicon glyphicon-remove text-danger del-item" aria-hidden="true"></span></td>                   
                        </tr>
                    <?php endforeach ?>
                        
                    <tr>
                        <td colspan="5">In total:</td>
                        <td><?= $session['cart.quantity'] ?></td>
                    </tr>
                    
                    <tr>
                        <td colspan="5">Amount:</td>
                        <td><?= $session['cart.sum'] ?></td>
                    </tr>
                    
                </tbody>
                
            </table>
            
        </div>

        <hr/>
        <?php $form = ActiveForm::begin() ?>
            <?= $form->field($order, 'name') ?>
            <?= $form->field($order, 'email') ?>
            <?= $form->field($order, 'phone') ?>
            <?= $form->field($order, 'address') ?>
            <?= Html::submitButton('Book', ['class' => 'btn btn-success']) ?>        
        <?php ActiveForm::end() ?>

    <?php else : ?>

        <h3>Cart is empty!</h3>

    <?php endif; ?>



</div>

