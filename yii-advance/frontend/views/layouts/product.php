<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use frontend\assets\ProductAsset;
use common\widgets\Alert;
use yii\helpers\Url;
use yii\bootstrap\Modal;

ProductAsset::register($this); //connecting files css, js, possition head or end

?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body>
<?php $this->beginBody() ?>

    <div id="fh5co-page">
	<header id="fh5co-header" class="inside" role="banner">
            <div class="container">
		<div class="header-inner">
                    <h1><a href="<?php echo Url::to(['/']); ?>">Watch4life<span class="text-purple">.</span></a></h1>
                    
			<nav role="navigation">
                            <ul>
				<li><a href="<?php echo Url::to(['watch/mans' ]); ?>">Mans</a></li>
				<li><a href="<?php echo Url::to(['watch/womans' ]); ?>">Womans</a></li>
                                <li><a href="<?php echo Url::to(['watch/kids' ]); ?>">Kids</a></li>
                                <?php if (Yii::$app->user->isGuest): ?>                               
                                    <li><a href="<?php echo Url::to(['log/login' ]); ?>">Login</a></li>
                                    <li><a href="<?php echo Url::to(['log/signup' ]); ?>">Signup</a></li>
                                <?php else: ?>
                                    <li><a href="<?php echo Url::to(['site/logout' ]); ?>">logout (<?php echo Yii::$app->user->identity->username; ?>)</a></li> 
                                <?php endif; ?>
                                <li><a href="<?php echo Url::to(['log/contact' ]); ?>">Contact</a></li>
				<li class="cta"><a href="<?php echo Url::to(['cart/view' ]); ?>">Add to cart</a></li>
                            </ul>
			</nav>
		</div>
            </div>
            
            
	</header> 
        
        
        <div class="dark-side"></div><!--Cgange color of sandwich-->
        
        <div class="container">
            
            <?=
            Breadcrumbs::widget([
                'itemTemplate' => "<li><i>{link}</i></li>\n", // template for all links
                'homeLink' => [
                    'label' => Yii::t('yii', 'Main'),
                    'url' => Yii::$app->homeUrl,
                     'class' => 'external',
                ],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [], 
   
            ])
            ?>
            <?= Alert::widget() ?>
            
        </div>
        
        
               <?php echo $content; ?> <!-- add dinamical content on a page-->
        
        
        <footer id="fh5co-footer" role="contentinfo">
            <div class="container">
		<div class="col-md-3 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
                    <h3>Contact Us</h3>
                    <p>+38(063)-078-54-97</p>
                    <p>Kiev, Sagaidachnogo 7, office 10</p>
		</div>
		<div class="col-md-6 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
                    <h3>Navigation</h3>
                    <ul class="float">
                        <li><a href="<?php echo Url::to(['watch/mans']); ?>">Mans</a></li>
                        <li><a href="<?php echo Url::to(['watch/womans']); ?>">Womans</a></li>
                        <li><a href="<?php echo Url::to(['watch/kids']); ?>">Kids</a></li>
                        <li><a href="<?php echo Url::to(['log/contact']); ?>">Contact</a></li>                      
                    </ul>
                    <ul class="float">
                        <?php if (Yii::$app->user->isGuest): ?>                               
                            <li><a href="<?php echo Url::to(['log/login']); ?>">Login</a></li>
                            <li><a href="<?php echo Url::to(['log/signup']); ?>">Signup</a></li>
                        <?php else: ?>
                            <li><a href="<?php echo Url::to(['site/logout']); ?>">logout (<?php echo Yii::$app->user->identity->username; ?>)</a></li> 
                        <?php endif; ?>
                        <li class="cta cart-href"><a href="#<?php echo Url::to(['cart/view']); ?>">Add to cart</a></li>
                    </ul>
		</div>
		<div class="col-md-2 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
                    <h3>Follow Us</h3>
                    <ul class="fh5co-social">
			<li><a href="#"><i class="icon-twitter"></i></a></li>
			<li><a href="https://www.facebook.com/profile.php?id=100010089948225"><i class="icon-facebook"></i></a></li>
			<li><a href="#"><i class="icon-google-plus"></i></a></li>
			<li><a href="#"><i class="icon-instagram"></i></a></li>
                    </ul>
		</div>
			
			
		<div class="col-md-12 fh5co-copyright text-center">
                    <p>&copy; 2017 Created by UNDERWATER. All rights reserved</p>	
		</div>
			
            </div>
	</footer>  
        
    </div>
    
<?php
Modal::begin([
    'header' => '<h2>Cart</h2>',
    'id' => 'cart',
    'size' => 'modal-lg',
    'footer' => '<button type="button" class="btn btn-default" data-dismiss="modal">Continue shopping</button>'
    . '<a href="' . Url::to(['cart/view']) . '" class="btn btn-primary">Checkout</a>'
    . '<button  type="button" class="btn btn-danger del-session" >Empty cart</button>',
]); 
Modal::end();
?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
