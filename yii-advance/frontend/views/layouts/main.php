<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\MainAsset;
use common\widgets\Alert;
use yii\helpers\Url;

MainAsset::register($this); //connecting files css, js, possition head or end

?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body>
<?php $this->beginBody() ?>

    <div id="fh5co-page">
	<header id="fh5co-header" role="banner">
            <div class="container">
		<div class="header-inner">
                    <h1><a href="<?php echo Url::to(['/']); ?>">Watch4life<span>.</span></a></h1>
                    
			<nav role="navigation">
                            <ul>
				<li><a href="<?php echo Url::to(['watch/mans' ]); ?>">Mans</a></li>
				<li><a href="<?php echo Url::to(['watch/womans' ]); ?>">Womans</a></li>
                                <li><a href="<?php echo Url::to(['watch/kids' ]); ?>">Kids</a></li>
                                <?php if (Yii::$app->user->isGuest): ?>                               
                                    <li><a href="<?php echo Url::to(['log/login' ]); ?>">Login</a></li>
                                    <li><a href="<?php echo Url::to(['log/signup' ]); ?>">Signup</a></li>
                                <?php else: ?>
                                    <li><a href="<?php echo Url::to(['site/logout' ]); ?>">logout (<?php echo Yii::$app->user->identity->username; ?>)</a></li> 
                                <?php endif; ?>
                                <li><a href="<?php echo Url::to(['log/contact' ]); ?>">Contact</a></li>
				<li class="cta"><a href="<?php echo Url::to(['cart/view' ]); ?>">Add to cart</a></li>
                            </ul>
			</nav>

		</div>
                       
            </div>
	</header>
        
        <aside id="fh5co-hero" class="js-fullheight">
            <div class="flexslider js-fullheight">
                <ul class="slides">
                    
                    <li style="background-image: url(/images/top_img/top-img-1.jpg);">
                        
		   	<div class="overlay-gradient"></div>
		   	<div class="container">
                            <div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
                                <div class="slider-text-inner">
                                    <h2>Start Your Day With This Whatch</h2>
                                    <p><a href="<?php echo Url::to(['/']); ?>" class="btn btn-primary btn-lg">Get started</a></p>
		   		</div>
                            </div>
		   	</div>
                        
		   </li>
		   <li style="background-image: url(/images/top_img/top-img-7.jpg);">
                       
		   	<div class="container">
                            <div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
		   		<div class="slider-text-inner">
                                    <h2>Take Your Business To The Next Level</h2>
                                    <p><a href="<?php echo Url::to(['/']); ?>" class="btn btn-primary btn-lg">Get started</a></p>
		   		</div>
                            </div>
		   	</div>
                       
		   </li>
		   <li style="background-image: url(/images/top_img/top-img-9.jpg);">
                       
                        <div class="container">
                            <div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
		   		<div class="slider-text-inner">
                                    <h2>You Think Different That Others Can't</h2>
                                    <p><a href="<?php echo Url::to(['/']); ?>" class="btn btn-primary btn-lg">Get started</a></p>
		   		</div>
                            </div>
		   	</div>
                       
		   </li>
		</ul>
            </div>
	</aside>
        
        
        <div id="fh5co-grid-products" class="animate-box">
            
            <div class="container">
		<div class="row">
                    <div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
			<h2>Don't waste your time</h2>
			<p>There are many ways to kill time - and none to revive him.</p>
                    </div>
		</div>
            </div>

            <div class="col-1">
		<a href="<?php echo Url::to(['watch/womans' ]); ?>" class="item-grid one" style="background-image: url(/images/top_img/woman-2.jpg)">
                    <div class="v-align">
			<div class="v-align-middle">
                            
                            <span class="icon"><img src="images/9.svg" alt="womans" class="img-responsive"></span>
                            <h3 class="title">Always be gorgeous</h3>
                            <h5 class="category">for woman</h5>
                            
			</div>
                    </div>
		</a>
		<a href="<?php echo Url::to(['watch/kids' ]); ?>" class="item-grid three" style="background-image: url(/images/top_img/kids-1.jpg)">
                    <div class="v-align">
                        <div class="v-align-middle">
                            
                            <span class="icon"><img src="images/18.svg" alt="kids" class="img-responsive"></span>
                            <h3 class="title">Age does not matter</h3>
                            <h5 class="category">for kids</h5>
                            
			</div>
                    </div>
		</a>
            </div>
            
            <div class="col-2">
                
                <a href="<?php echo Url::to(['watch/mans' ]); ?>" 
                class="item-grid two" style="background-image: url(/images/top_img/man-2.jpg)">
                    
                    <div class="v-align">
			<div class="v-align-middle">
                            
                            <span class="icon"><img src="images/27.svg" alt="Mans" class="img-responsive"></span>
                            <h3 class="title">Follow your dream</h3>
                            <h5 class="category">for man</h5>
                            
			</div>
                    </div>
		</a>			
            </div>
			
	</div>
        
        
                                    
        <footer id="fh5co-footer" role="contentinfo">
            <div class="container">
		<div class="col-md-3 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
                    <h3>Contact Us</h3>
                    <p>+38(063)-078-54-97</p>
                    <p>Kiev, Sagaidachnogo 7, office 10</p>
                </div>
                <div class="col-md-6 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
                    <h3>Navigation</h3>
                    <ul class="float">
                        <li><a href="<?php echo Url::to(['watch/mans']); ?>">Mans</a></li>
                        <li><a href="<?php echo Url::to(['watch/womans']); ?>">Womans</a></li>
                        <li><a href="<?php echo Url::to(['watch/kids']); ?>">Kids</a></li>
                        <li><a href="<?php echo Url::to(['log/contact']); ?>">Contact</a></li>                      
                    </ul>
                    <ul class="float">
                        <?php if (Yii::$app->user->isGuest): ?>                               
                            <li><a href="<?php echo Url::to(['log/login']); ?>">Login</a></li>
                            <li><a href="<?php echo Url::to(['log/signup']); ?>">Signup</a></li>
                        <?php else: ?>
                            <li><a href="<?php echo Url::to(['site/logout']); ?>">logout (<?php echo Yii::$app->user->identity->username; ?>)</a></li> 
                        <?php endif; ?>
                        <li class="cta cart-href"><a href="#<?php echo Url::to(['cart/view']); ?>">Add to cart</a></li>
                    </ul>
                </div>
                <div class="col-md-2 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
                    <h3>Follow Us</h3>
                    <ul class="fh5co-social">
                        <li><a href="#"><i class="icon-twitter"></i></a></li>
                        <li><a href="https://www.facebook.com/profile.php?id=100010089948225"><i class="icon-facebook"></i></a></li>
                        <li><a href="#"><i class="icon-google-plus"></i></a></li>
                        <li><a href="#"><i class="icon-instagram"></i></a></li>
                    </ul>
                </div>
			
		<div class="col-md-12 fh5co-copyright text-center">
                    <p>&copy; 2017 Created by UNDERWATER. All rights reserved</p>	
		</div>
			
            </div>
	</footer>       
    </div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
