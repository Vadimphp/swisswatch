<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class MainAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bold//icomoon.css',
        'css/bold/bootstrap.css',
        'css/bold/flexslider.css',
        'css/main.min.css',
        'css/bold/style.css',
        'css/fonts.min.css',
        
        
        
    ];
    public $js = [        
        'js/bold/jquery.min.js',
	'js/animate/animate-css.js',
	'js/bold/jquery.easing.1.3.js',
	'js/bold/bootstrap.min.js',
	'js/bold/jquery.waypoints.min.js',
        'js/bold/main.js',
	'js/bold/jquery.flexslider-min.js',
        'js/common.js', //there is modernizr

    ];
    
    public $jsOptions = [
       // 'position' => \yii\web\View::POS_HEAD
    ];

    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset', // Disable in a new topic
    ];
}
