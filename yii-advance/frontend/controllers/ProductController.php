<?php

namespace frontend\controllers;

use Yii; 
use yii\web\Controller;
use frontend\models\Product;
use frontend\models\Brands;
use frontend\models\Comment;
use frontend\models\Compare;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;


class ProductController extends Controller
{
    
    /**
     *
     * @var translation ProductAsset to this page
     * @var $mail get womans or mans product
     * @var $filter get product by price and brands
     * @var $comment add comment to product
     */
    public $layout = 'product'; 
    
    public function actionList()
    {            
        $mail = Yii::$app->request->get('mail');
        
        $urlParam = new Product();
        $conditional = $urlParam->choseMail($mail);
        
        $getBrand = Yii::$app->request->get('brand');
        $filter = Brands::find()->all();

        $product = Product::find()->all(); //without ActiveDataProvider
        
        $getBrand = Yii::$app->request->get('brand');
        $query = $urlParam->filter($conditional, $getBrand);
        
        $nameMin = $urlParam->price();
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,          
            'pagination' => [
                'pageSize' => 9,
                'forcePageParam' => false,
                'pageSizeParam' => false,   
            ]
        ]);
     
        return $this->render('list', [
            'product' => $product, //вывод без ActiveDataProvider
            'dataProvider' => $dataProvider,
            'mail' => $mail,
            'filter' => $filter,
            'getBrand' => $getBrand,
            'nameMin' => $nameMin,
            //'name_max' => $nameMax,
        ]);
        
    }
    
    public function actionView() 
    {   
        
        $id = Yii::$app->request->get('id'); 
        
        $conditional['id'] = $id;
        
        $product = Product::find()->where($conditional)->one();  
       
        $session = Yii::$app->session;
        $session->set('id', $id);
        
        $commentsList = Comment::find()->where(['product_id' => $product->id])->all();         
        $commentModel = new Comment();
        $commentModel->product_id = $product->id;
        
        
        //$compareSend = new Compare();
        //$compareSend->product_id = $product->id;

             
        return $this->render('view', [
            'product' => $product,
            'commentsList' => $commentsList,
            'commentModel' => $commentModel,
            //'compareSend' => $compareSend,
            'product_id' => Yii::$app->session->get('product_id'),
            'id' => Yii::$app->session->get('id'),
        ]);
    }
    
    /**
     * 
     * @return ajax comment validate and save form data 
     */
   public function actionValidateComment()
    {
        if (!Yii::$app->request->isAjax) {
            return;
        }
        
        $model = new Comment();
        $model->load(Yii::$app->request->post());
        
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        if ($model->validate()) {
            return ['success' => true];
        } else {
            return ActiveForm::validate($model);
        }
    }
    
    public function actionSaveComment()
    {
        if (!Yii::$app->request->isAjax) {
            return;
        }
        
        $model = new Comment();
        $model->load(Yii::$app->request->post());
        
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        if ($model->save()) {
            return ['success' => true];
        }
        
    }
   

}
