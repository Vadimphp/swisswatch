<?php

namespace frontend\controllers;

use Yii; 
use \yii\web\Controller;
use frontend\models\Product;
use frontend\models\Cart;
use frontend\models\ProductOrder;
use frontend\models\OrderItem;

/**
 * Description of CartController
 *
 * @author Vadim
 */

class CartController extends Controller
{
    
    public $layout = 'product';
    
    public function actionAdd() 
    {
       $quantity = (int)Yii::$app->request->get('quantity');
       $quantity = !$quantity ? 1 : $quantity;
        
       $id = Yii::$app->request->get('id');
       $product = Product::findOne($id);
       if (empty($product)) {
           return false;
       } 
       
       $session = Yii::$app->session;
       $session->open();   
       $cart = new Cart();
       $cart->addToCart($product, $quantity);
       
       if(!Yii::$app->request->isAjax) {
           return $this->redirect(Yii::$app->request->referrer);
       }
       
       $this->layout = false;
       
       return $this->render('cart-modal', [
           'session' => $session,
       ]);
   
    }
    
    public function actionClear () 
    {
       $session = Yii::$app->session;
       $session->open();
       $session->remove('cart');
       $session->remove('cart.quantity');
       $session->remove('cart.sum');
       
       $this->layout = false;
       
       return $this->render('cart-modal', [
           'session' => $session,
       ]);
    }
    
    public function  actionDelItem ()
    {
        $id = Yii::$app->request->get('id');       
        $session = Yii::$app->session;
        $session->open();
        $cart = new Cart();
        $cart->recalculation($id);
        
        $this->layout = false;
        
        return $this->render('cart-modal', [
           'session' => $session,
       ]);
    }

    public function  actionShow ()
    {
        $session = Yii::$app->session;
        $session->open();
        
        $this->layout = false;
        
        return $this->render('cart-modal', [
           'session' => $session,
       ]);
    }
    
    public function  actionView ()
    {
        $session = Yii::$app->session;
        $session->open();
      
        $order = new ProductOrder ();
        
        if($order->load(Yii::$app->request->post()) ) {
            $order->quantity = $session['cart.quantity'];
            $order->sum = $session['cart.sum'];
       
            if($order->save()) {
                
                $this->saveOrderItem($session['cart'], $order->id);
                
                Yii::$app->session->setFlash('success', 'Your order is accepted, soon manager will contact you');
                
                
                $send_order = new Order();
                //отправка почты на мейл клиента от мейла администратора  https://yiiframework.com.ua/ru/doc/guide/2/tutorial-mailing/
                $send_order->sendMail($order->email);
//                Yii::$app->mailer->compose('order', ['session' => $session]) // order it is php file
//                    ->setFrom(['vadymbondarenko91@gmail.com' => 'watch4life'])
//                    ->setTo($order->email)
//                    ->setSubject('Order from site')
//                    ->send();
                
                //отправка почты на мейл администратора можно поменять вид в отлиичии от клиента
                $send_order->sendMail(Yii::$app->params['adminEmail']);
//                Yii::$app->mailer->compose('order', ['session' => $session]) // order it is php file
//                    ->setFrom(['vadymbondarenko91@gmail.com' => 'watch4life'])
//                    ->setTo(Yii::$app->params['adminEmail'])
//                    ->setSubject('Order from site')
//                    ->send();

                //если все сохранено очищаем корзину можно добаить транзакции  ActiveRecord в случае если что-то не сохранилось
                $session->remove('cart');
                $session->remove('cart.quantity');
                $session->remove('cart.sum');
                
                
               return $this->refresh(); 
               
            } else {
                Yii::$app->session->setFlash('error', 'Error Ordering');
            }
        }
        
        return $this->render('view', [
           'session' => $session,
           'order' => $order,
        ]);
    }
    
    protected function saveOrderItem ($items, $order_id)
    {
        foreach ($items as $id => $item){
            $order_item = new OrderItem();
            $order_item->order_id = $order_id;
            $order_item->product_id = $id;
            $order_item->brand_name = $item['brand_name'];
            $order_item->price = $item['price'];
            $order_item->quantity_item = $item['quantity'];
            $order_item->sum_item = $item['quantity'] * $item['price'];
            $order_item->save();
        }
    }
}
