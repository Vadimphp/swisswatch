<!-- form to send a comment -->
<?php
    use yii\helpers\Url;
?>

<script type="text/javascript">
    $(document).ready(function () {
        var $form = $('#commentForm');

        $form.on('beforeSubmit', function () {
            var data = $(this).serialize();
            $.post('<?php echo Url::to(['save-comment']); ?>', data, function (response) {
                if (response.success) {
                    $('#comment-text').val('');
                    $('#comment-name').val('');

                    $.pjax.reload({container: '#comment', 'timeout': 5000});
                }
            });
            return false;
        });

    });
</script>