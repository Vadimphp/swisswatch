<div class="table-responsive">
    <table style="width: 100%; border: 1px solid #ddd; border-collapse: collapse;">
            <thead>
                <tr style="background: #f9f9f9;">
                    
                    <th style="padding: 8px; border: 1px solid #ddd;">Бренд</th>
                    <th style="padding: 8px; border: 1px solid #ddd;">Количество</th>
                    <th style="padding: 8px; border: 1px solid #ddd;">Цена</th>
                    <th style="padding: 8px; border: 1px solid #ddd;">Сумма</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($session['cart'] as $id => $item) : ?>
                <tr>
                    
                    <td style="padding: 8px; border: 1px solid #ddd;"><?= $item['brand_name'] ?></td>
                    <td style="padding: 8px; border: 1px solid #ddd;"><?= $item['quantity'] ?></td>
                    <td style="padding: 8px; border: 1px solid #ddd;"><?= $item['price'] ?></td>
                    <td style="padding: 8px; border: 1px solid #ddd;"><?= $item['price'] * $item['quantity'] ?></td>                   
                </tr>
                <?php endforeach?>
                <tr>
                    <td colspan="4" style="padding: 8px; border: 1px solid #ddd;">Итого:</td>
                    <td style="padding: 8px; border: 1px solid #ddd;"><?= $session['cart.quantity'] ?></td>
                </tr>
                <tr>
                    <td colspan="4" style="padding: 8px; border: 1px solid #ddd;">На сумму:</td>
                    <td style="padding: 8px; border: 1px solid #ddd;"><?= $session['cart.sum'] ?></td>
                </tr>
            </tbody>
        </table>
    </div>



