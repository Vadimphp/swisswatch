<?php

namespace backend\models;

use backend\models\Product;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "description".
 *
 * @property integer $id
 * @property string $specification
 * @property string $description
 */
class Description extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'description';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['specification', 'description'], 'required'],
            [['specification', 'description'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'specification' => 'Specification',
            'description' => 'Description',
        ];
    }
    
    public function getProductOption ()
    {
        $className = Product::className();
        $link = ['id' => 'id',];
        
        return $this->hasOne($className, $link);
    }
}
