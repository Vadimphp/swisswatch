<?php

namespace backend\models;


use backend\models\ProductOrder;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "order_item".
 *
 * @property string $id
 * @property string $order_id
 * @property string $product_id
 * @property string $brand_name
 * @property double $price
 * @property integer $quantity_item
 * @property double $sum_item
 */
class OrderItem extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id', 'brand_name', 'price', 'quantity_item', 'sum_item'], 'required'],
            [['order_id', 'product_id', 'quantity_item'], 'integer'],
            [['price', 'sum_item'], 'number'],
            [['brand_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'product_id' => 'Product ID',
            'brand_name' => 'Brand Name',
            'price' => 'Price',
            'quantity_item' => 'Quantity Item',
            'sum_item' => 'Sum Item',
        ];
    }
    
    public function getProductOrder ()
    {
        $className = ProductOrder::className();
        $link = ['id' => 'order_id',];
        
        return $this->hasOne($className, $link);
    }
}
