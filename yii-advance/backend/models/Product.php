<?php

namespace backend\models;

use Yii;
use backend\models\Description;
use backend\models\ProductOption;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $main_path_to_file
 * @property string $brand_name
 * @property string $model
 * @property integer $product_code
 * @property integer $price
 * @property string $currency
 * @property integer $status
 * @property string $mail
 * @property integer $sort_order
 * @property integer $is_in
 */
class Product extends ActiveRecord
{
    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }
    //const MAIL_MAN = 'man';
    //const MAIL_WOMAN = 'woman';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['main_path_to_file', 'brand_name', 'model', 'product_code', 'price', 'currency', 'status', 'mail', 'sort_order', 'is_in'], 'required'],
            [['product_code', 'price', 'status', 'sort_order', 'is_in'], 'integer'],
            [['main_path_to_file', 'brand_name', 'model', 'currency'], 'string', 'max' => 255],
            [['mail'], 'string', 'max' => 49],
            [['image'], 'file', 'extensions' => 'png, jpg'],
            [['gallery'], 'file', 'extensions' => 'png, jpg', 'maxFiles' => 4],//перенести в галерею
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '№ ID',
            //'main_path_to_file' => 'Main Path To File',
            'image' => 'Photo',
            'gallery' => 'Gallery',
            'brand_name' => 'Brand Name',
            'model' => 'Model',
            'product_code' => 'Product Code',
            'price' => 'Price',
            'currency' => 'Currency',
            'status' => 'Status',
            'mail' => 'Mail',
            'sort_order' => 'Sort Order',
            'is_in' => 'Is In',
        ];
    }
    
    public function getOptionsfrom () 
    {
        $className = ProductOption::className();
        $link = ['product_id' => 'id'];
        
        return $this->hasMany($className, $link)->limit(17);
    }
    
   
    public $image;
    public $gallery; 
    
    public function upload()
    {
        if($this->validate()) {
            $path = 'upload/store/' . $this->image->baseName . '.' . $this->image->extension;
            $this->image->saveAs($path);
            $this->attachImage($path, true);
            unlink($path);
            return true;
        } else {
            return false;
        }
    }
    
    public function uploadGallery()//перенести в галерею
    {
        if($this->validate()) {
            foreach ($this->gallery as $file) {
                $path = 'upload/store/' . $file->baseName . '.' . $file->extension;
                $file->saveAs($path);
                $this->attachImage($path);
                unlink($path);               
            }    
            return true;
        } else {
            return false;
        }
    }
    
    public function getDescription ()
    {
        $className = Description::className();
        $link = ['id' => 'id',];
        
        return $this->hasOne($className, $link);
    }

}
