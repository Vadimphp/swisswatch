<?php

namespace backend\models;

use backend\models\Option;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "product_option".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $option_id
 * @property string $product_value
 */
class ProductOption extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_option';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'option_id', 'product_value'], 'required'],
            [['product_id', 'option_id'], 'integer'],
            [['product_value'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'option_id' => 'Option ID',
            'product_value' => 'Product Value',
        ];
    }
    
    public function getOption()
    {
        $className = Option::className();
        $link = ['id' => 'option_id',];
        
        return $this->hasOne($className, $link);
    }
    
    public function getName()
    {
        return $this->option->option_name;
    }
}
