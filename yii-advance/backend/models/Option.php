<?php

namespace backend\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "option".
 *
 * @property integer $id
 * @property string $option_name
 * @property integer $sort_order
 */
class Option extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'option';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['option_name', 'sort_order'], 'required'],
            [['sort_order'], 'integer'],
            [['option_name'], 'string', 'max' => 49],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'option_name' => 'Option Name',
            'sort_order' => 'Sort Order',
        ];
    }
}
