<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\ProductOrder */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Product Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-order-view">

    <h1>Order №<?= $model->id ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'created_at',
            'updated_at',
            'quantity',
            'sum',
            [
                'attribute' => 'status',
                'value' =>  !$model->status ? '<span class="text-danger">Active</span>' 
                            : '<span class="text-success">Close</span>',
                'format' => 'html',
            ],
            //'status',
            'name',
            'email:email',
            'phone',
            'address',
        ],
    ]) ?>

        <?php $items = $model->orderItem; ?>
    
    
    <div class="table-responsive">
        <table class="table table-hover table-striped">
            <thead>
                <tr>
        
                    <th>Brand</th>
                    <th>Qantity</th>
                    <th>Price</th>
                    <th>Summa</th>
                   
                </tr>
            </thead>
            <tbody>
                <?php foreach ($items as $item) : ?>
                <tr>
                    <td>
                        <a href="<?php echo yii\helpers\Url::to(['/product/view', 'id' => $item->product_id, ]); ?>">
                            <?= $item['brand_name'] ?>
                        </a>
                    </td>                     
                   
                    <td><?= $item['quantity_item'] ?></td>
                    <td><?= $item['price'] ?></td>
                    <td><?= $item['sum_item'] ?></td>
                </tr>
                <?php endforeach?>
              
            </tbody>
        </table>
    </div>

</div>

    
