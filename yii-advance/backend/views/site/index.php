<?php

use yii\helpers\Url;

$this->title = 'Whatch4Life';
?>
<div class="container">
    <div class="row">
        <div class="col-sm-4 col-md-3 ">
            <a href="<?php echo Url::to(['product/index']); ?>" class="box-to">
                <div class="url-to">
                    <h3>Product</h3>
                </div>
            </a>
        </div>
        <div class="col-sm-4 col-md-3 ">
            <a href="<?php echo Url::to(['product-order/index']); ?>" class="box-to">
                <div class="url-to">
                    <h3>ProductOrder</h3>
                </div>
            </a>
        </div>
        <div class="col-sm-4 col-md-3 ">
            <a href="<?php echo Url::to(['order-item/index']); ?>" class="box-to">
                <div class="url-to">
                    <h3>OrderItem</h3>
                </div>
            </a>
        </div>
        <div class="col-sm-4 col-md-3 ">
            <a href="<?php echo Url::to(['option/index']); ?>" class="box-to">
                <div class="url-to">
                    <h3>Option</h3>
                </div>
            </a>
        </div>
        <div class="col-sm-4 col-md-3 ">
            <a href="<?php echo Url::to(['product-option/index']); ?>" class="box-to">
                <div class="url-to">
                    <h3>ProductOption</h3>
                </div>
            </a>
        </div>
        <div class="col-sm-4 col-md-3 ">
            <a href="<?php echo Url::to(['description/index']); ?>" class="box-to">
                <div class="url-to">
                    <h3>Description</h3>
                </div>
            </a>
        </div>
    </div>
</div>
