<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Product', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'main_path_to_file',
            'brand_name',
            'model',
            //'image',
            [
                'attribute' => 'image',
                'value' => function($model) {

                return '<img src="'.$model->getImage()->getUrl().'" width="60" />';
                
                },
                'format' => 'html',
            ],
            //'product_code',
             'price',
             'currency',
            // 'status',
             'mail',
            // 'sort_order',
            // 'is_in',
            [
                'attribute' => 'is_in',
                'value' => function ($data) {
                    return !$data->is_in ? '<span class="text-danger">No</span>' 
                            : '<span class="text-success">Yes</span>';
                    },
                'format' => 'html',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
