<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Product */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    
    <?php $img = $model->getImage(); ?>
    
    <?php $gallery = $model->getImages(); ?> <!--перенести в галерею-->
    
    <?php $items = $model->description; ?>
    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            //'main_path_to_file',
            [
                'attribute' => 'image',
                'value' =>  "<img src='{$img->getUrl()}'>",
                'format' => 'html',
            ],
            // 'gallery',           
            'brand_name',
            'model',
            'product_code',
            'price',
            'currency',
            'status',
            'mail',
            'sort_order',
            //'is_in',
            [
                'attribute' => 'is_in',
                'value' =>  !$model->is_in ? '<span class="text-danger">No</span>' 
                            : '<span class="text-success">Yes</span>',
                'format' => 'html',
            ],
        ],
    ]) ?>
    

    

    <div class="table-responsive">
        <table class="table table-hover table-striped">
            <thead>
                <tr>   
                    <th><a href="<?php echo Url::to(['description/update', 'id' => $model -> id, ]); ?>" class="box-to">Specification</a></th>
                    <th>Description</th>                  
                </tr>
            </thead>
            <tbody>
                <tr>                                    
           <td><?= $items['specification'] ?></td>
                    <td><?= $items['description'] ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    
    <?php $options = $model->optionsfrom; ?>
    
    <hr>
    <br>
    <div class="table-responsive">
        <table class="table table-hover table-striped">
            <thead>
                <tr>   
                    <th>Option Name</th>
                    <th>Product Value</th>                  
                </tr>
            </thead>
            <tbody>
                <?php foreach ($options as $option): ?>
                    <tr>
                        <td class="text-left"><span class="color">
                                <?php echo $option->option_id; ?> | <?php echo $option->getName(); ?></span>
                        </td>
                        <td class="text-left">
                            <?php echo $option->product_value; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>

            </tbody>
        </table>
    </div>

</div>
