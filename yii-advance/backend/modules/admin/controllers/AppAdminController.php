<?php

namespace backend\modules\admin\controllers;

use yii\web\Controller;
use yii\filters\AccessControl;
/**
 * Description of AppAdminController
 *
 * @author Вадим
 */
class AppAdminController extends Controller 
{
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' =>true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
        
    }
}
