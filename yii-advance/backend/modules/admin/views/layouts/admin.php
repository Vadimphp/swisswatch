<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use backend\assets\ProductAsset;
use common\widgets\Alert;
use yii\helpers\Url;
use yii\bootstrap\Modal;


ProductAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

    <div id="fh5co-page">
	<header id="fh5co-header" class="inside" role="banner">
            <div class="container">
		<div class="header-inner">
                    <h1><a href="#">Bold<span>.</span></a></h1>
                    
			<nav role="navigation">
                            <ul>
				<li><a href="products.html">Products</a></li>
				<li><a href="services.html">Services</a></li>
                                <li><a href="#" class="cart-href">Pricing</a></li>
				<li><a href="about.html">About</a></li>
				<li><a href="contact.html">Contact</a></li>
				<li class="cta"><a href="#">Get started</a></li>
                            </ul>
			</nav>
                    
		</div>
            </div>
	</header>
        
        <div class="container">
            <?=
            Breadcrumbs::widget([
                'itemTemplate' => "<li><i>{link}</i></li>\n", // template for all links
                'homeLink' => [
                    'label' => Yii::t('yii', 'Main'),
                    'url' => Yii::$app->homeUrl,
                     'class' => 'external',
                ],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [], 
   
            ])
            ?>
            <?= Alert::widget() ?>
        </div>
        
        <div class="container">
               <?php echo $content; ?>
        </div>
        
        <footer id="fh5co-footer" role="contentinfo">
            <div class="container">
		<div class="col-md-3 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
                    <h3>About Us</h3>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                    <p><a href="#" class="btn btn-primary btn-outline with-arrow btn-sm">I'm button <i class="icon-arrow-right"></i></a></p>
		</div>
		<div class="col-md-6 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
                    <h3>Our Services</h3>
                    <ul class="float">
			<li><a href="#">Web Design</a></li>
			<li><a href="#">Branding &amp; Identity</a></li>
			<li><a href="#">Free HTML5</a></li>
			<li><a href="#">HandCrafted Templates</a></li>
                    </ul>
			<ul class="float">
			<li><a href="#">Free Bootstrap Template</a></li>
			<li><a href="#">Free HTML5 Template</a></li>
			<li><a href="#">Free HTML5 &amp; CSS3 Template</a></li>
			<li><a href="#">HandCrafted Templates</a></li>
                    </ul>
		</div>
		<div class="col-md-2 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
                    <h3>Follow Us</h3>
                    <ul class="fh5co-social">
			<li><a href="#"><i class="icon-twitter"></i></a></li>
			<li><a href="#"><i class="icon-facebook"></i></a></li>
			<li><a href="#"><i class="icon-google-plus"></i></a></li>
			<li><a href="#"><i class="icon-instagram"></i></a></li>
                    </ul>
		</div>
			
			
		<div class="col-md-12 fh5co-copyright text-center">
                    <p>&copy; 2016 Free HTML5 template. All Rights Reserved. <span>Designed with <i class="icon-heart"></i> by <a href="http://freehtml5.co/" target="_blank">FreeHTML5.co</a> Colored Icons by <a href="https://dribbble.com/TrinhHo" target="_blank">Trinh Ho</a> Demo Images by <a href="http://unsplash.com/" target="_blank">Unsplash</a></span></p>	
		</div>
			
            </div>
	</footer>  
        
    </div>
<?php
Modal::begin([
    'header' => '<h2>Корзина</h2>',
    'id' => 'cart',
    'size' => 'modal-lg',
    'footer' => '<button type="button" class="btn btn-default" data-dismiss="modal">Продолжить покупку</button>'
    . '<a href="' . Url::to(['cart/view']) . '" class="btn btn-primary">Оформить заказ</a>'
    . '<button  type="button" class="btn btn-danger del-session" >Очистить корзину</button>',
]); 
Modal::end();
?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>


