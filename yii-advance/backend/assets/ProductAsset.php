<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class ProductAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bold//icomoon.css',
        'css/bold/bootstrap.css',
        'css/bold/style.css',
        'css/fonts.min.css',
        'css/main.min.css',
        'css/tab_css/style.css',
        
    ];
     public $js = [      
        'js/bold/jquery.min.js',
        'js/bold/modernizr-2.6.2.min.js',
	'js/animate/animate-css.js',
	'js/bold/jquery.easing.1.3.js',
	'js/bold/bootstrap.min.js',
	'js/bold/jquery.waypoints.min.js',
        'js/bold/main.js',
        'js/tab_js/js-product.js',
    
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];
    
    
    
    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset', // отключить в новой теме
    ];
}
